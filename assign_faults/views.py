from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.decorators import *
from django.http import HttpResponse
from django.utils import timezone
from django.core.paginator import Paginator
from django.shortcuts import redirect, render, get_object_or_404


from company_profile.models import CompanyProfile
from assign_faults.models import AssignFault
from rest_api.models import ProjectCase
from assign_faults.models import AssignFault
from field_worker.models import FieldWorker

def list_of_assigned_faults(request):
	assigned_faults = AssignFault.objects.all().order_by('-id')
	paginator = Paginator(assigned_faults, 9)
	try:
		page = int(request.GET.get('page', '1'))
	except:
		page = 1
	try:
		assigned_faults = paginator.page(page)
	except(EmptyPage, InvalidPage):
		assigned_faults = paginator.page(paginator.num_pages)
	context = {
	'assigned_faults': assigned_faults
	}
	return render(request, 'assign_faults/list_of_assigned_faults.html', context)


# list assigned faults on field worker profile
def list_of_assigned_faults_to_fieldworker(request, pk):
	field_worker = get_object_or_404(FieldWorker, pk=pk)
	assigned_faults = AssignFault.objects.filter(field_worker=field_worker).order_by('-id')
	context = {
	'assigned_faults': assigned_faults
	}
	return assigned_faults


def assigned_faults_to_fieldworker_list(request):
	assigned_faults = AssignFault.objects.filter(field_worker=field_worker).order_by('-id')
	context = {
	'assigned_faults_to_field_worker_list': assigned_faults
	}
	return assigned_faults