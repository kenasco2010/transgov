from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponseForbidden
from rest_framework.decorators import (api_view, permission_classes, list_route)
from django.core.validators import validate_email
from rest_framework.serializers import ModelSerializer

from assign_faults.models import AssignFault

class AssignFaultSerializer(serializers.ModelSerializer):

	class Meta:
		model = AssignFault
		fields = ('id','community_fault','field_worker','date_assigned','created_date')