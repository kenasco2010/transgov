from django import forms
from django.forms import TextInput
from django.contrib.auth.models import User
from django.contrib.auth.models import *
from assign_fault.models import AssignFault


class AssignFaultForm(forms.ModelForm):
   

    class Meta:
        model = AssignFault
        fields = ['community_fault', 'field_worker', 'date_assigned']
