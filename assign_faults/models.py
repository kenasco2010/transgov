from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from imagekit.models import ProcessedImageField
from rest_api.models import ProjectCase
from field_worker.models import FieldWorker
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class AssignFault(models.Model):
    community_fault = models.ForeignKey(ProjectCase, related_name='community_fault', blank=True, null=True)
    field_worker = models.ForeignKey(FieldWorker, related_name='assigned_field_worker', blank=True, null=True)
    date_assigned =  models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    @property
    def region(self):
        return self.community_fault.region.region

    @property
    def suburb(self):
        return self.community_fault.suburb.suburb

    def __unicode__(self):
        return self.community_fault.case_name

    def __str__(self):
        return self.community_fault.case_name

    class Meta:
            verbose_name_plural = "Faults Assigned"
