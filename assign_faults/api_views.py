# from django.shortcuts import render

from django.contrib.auth import authenticate, login, logout, get_user_model
# from django.contrib.auth.models import User

# Django send emails
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import pre_save, post_save

from django.utils import timezone

from rest_framework import viewsets
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_api.serializers import *
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import FileUploadParser

import re
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view

from assign_faults.models import AssignFault
from assign_faults.serializers import AssignFaultSerializer
from rest_api.views import StandardResultsSetPagination #pagination from rest views


class AssignFaultViewSet(viewsets.ModelViewSet):
	model= AssignFault
	serializer_class = AssignFaultSerializer
	queryset = AssignFault.objects.all()
	permission_classes = (AllowAny,)

	@list_route(methods=('GET',), url_path="field-workers-faults", 
		permission_classes=(IsAuthenticated,))
	def field_workers_faults_assigned(self, request, *args, **kwargs):
		get_fieldworker_faults = AssignFault.objects.filter(author=request.user).order_by('-id')
		fieldworker_faults = self.serializer_class(get_fieldworker_faults, many=True)
		if get_fieldworker_faults:
			return Response ({
				'status_code': status.HTTP_200_OK,
				'message': 'All faults assigned to you',
				'results': fieldworker_faults.data
				})
		else:
			return Response ({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'You have no fault assigned to you'
				})