from __future__ import unicode_literals

from django.apps import AppConfig


class AssignFaultsConfig(AppConfig):
    name = 'assign_faults'
