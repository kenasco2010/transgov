from django.conf.urls import include, patterns, url
from api.views import *

urlpatterns = [
url(r'^project-details/(?P<pk>\d+)/subscription/unsub$', subscribe, name='internalApi'),
url(r'^(?P<service>[\w\-]+)/(?P<resource>[\w\-]+)/(?P<incomingParams>[\w\-\{\}\:\"]+)$', apiHandler, name='internalApi'),

]
