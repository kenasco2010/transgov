from django.http import JsonResponse
import json
import importlib
# Create your models here.
from project.views import *
def subscribe(request,pk):
    response = delete_subscriber(request, pk)
    return JsonResponse(response)

def apiHandler(request, service, resource, incomingParams=None):
    params = json.loads(incomingParams)
    app = importlib.import_module(service+'.views', package=None)
    response = getattr(app, resource)(request, **params)
    return JsonResponse(response)
