from django.contrib import admin

# Register your models here.

from law_digest.models import DigestCategory, DigestContent, ContentServed

backendContentModels = [DigestCategory, DigestContent, ContentServed]
admin.site.register(backendContentModels)
