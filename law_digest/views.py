from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.dispatch import receiver
from django.db.models.signals import post_save


from django.template.loader import get_template
from django.template import Context
from django.forms import formset_factory
import requests
from gov import credInfo
from law_digest.forms import *
from law_digest.models import DigestCategory, DigestTopic, DigestContent
from rest_api.notifications import get_firebase
from law_digest.mobile_notifications import *

# Create your views here.

def add_digest_category(request):
	if request.method == 'POST':
		cat_form = DigestCategoryForm(request.POST, request.FILES)
		if cat_form.is_valid():
			form =  cat_form.save(commit=False)
			form.author = request.user
			form.save()
			return redirect('category_list')
	else:
		form = DigestCategoryForm()
	return render(request, 'law_digest/add_digest_category.html', {'form':form})


def edit_digest_category(request, pk):
	category = get_object_or_404(DigestCategory, pk=pk)
	if request.method == "POST":
		category_form = DigestCategoryForm(request.POST, request.FILES, instance=category)
		if category_form.is_valid():
			category_form = category_form.save(commit=False)
			category_form.author = request.user
			category_form.save()
			return redirect('category_list')
	else:
		category_form = DigestCategoryForm(instance=category)
	return render (request, 'law_digest/edit_category_digest.html', {'form': category_form})


def delete_digest_category(request, pk):
	category = get_object_or_404(DigestCategory, pk=pk)
	category.delete()
	return redirect('category_list') 
	

def digest_category_list(request):
	category_list = DigestCategory.objects.all().order_by('-id')
	context = {
		'categories': category_list
	}
	return render(request, 'law_digest/category_list.html', context)


	############################ Code not in use ##################################
def add_topic(request):
	if request.method == 'POST':
		add_topic_form = DigestTopicForm(request.POST)
		if add_topic_form.is_valid():
			topic_form = add_topic_form.save(commit=False)
			topic_form.author = request.user
			topic_form.save()
			return redirect('add_topic')
	else:
		topic_form = DigestTopicForm()
	return render(request, 'law_digest/add_topic.html', {'form':topic_form})

def topic_list(request):
	topic_list = DigestTopic.objects.all().order_by('-id')
	context = {
	'topics': topic_list
	}
	return render(request, 'law_digest/topic_list.html', context)
######################## End of code not in use ############################


def select_category_list_to_add_content(request):
	category_list = DigestCategory.objects.all().order_by('-id')
	context = {
	'categories': category_list
	}
	return render(request, 'law_digest/select_category_to_add_content.html', 
		context)


def add_content(request, pk):
	category = get_object_or_404(DigestCategory, pk=pk)
	if request.method == 'POST':
		content_form = DigestContentForm(request.POST, request.FILES)
		if content_form.is_valid():
			form = content_form.save(commit=False)
			form.content_category = category
			form.author = request.user
			form.save()
			return redirect('add_digest_content', category.id)
			# return redirect('add_digest_content')
	else:
		form = DigestContentForm()
		context = {
		'category': category,
		'form': form
		}
		return render(request, 'law_digest/add_content.html', context)


def view_content(request, pk):
	category = get_object_or_404(DigestCategory, pk=pk)
	content = DigestContent.objects.filter(content_category=category).order_by('-id')
	context = {
	'category': category,
	'content': content
	}
	return render(request, 'law_digest/view_content.html', context)


def edit_content(request, pk):
	content = get_object_or_404(DigestContent, pk=pk)
	return_to_content_list = content.content_category.id
	category = content.content_category.name
	if request.method == 'POST':
		content_form = DigestContentForm(request.POST, request.FILES, instance=content)
		if content_form.is_valid():
			content_form = content_form.save(commit=False)
			content_form.author = request.user
			content_form.save()
			return redirect('view_content', return_to_content_list)
	else:
		content_form = DigestContentForm(instance=content)
		context = {
		'form': content_form,
		'category': category
		}
	return render (request, 'law_digest/edit_content.html', context)


def delete_digest_content(request, pk):
	content = get_object_or_404(DigestContent, pk=pk)
	return_to_content_list = content.content_category.id
	content.delete()
	return redirect('view_content', return_to_content_list) 
