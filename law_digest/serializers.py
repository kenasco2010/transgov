
from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponseForbidden
from rest_framework.decorators import (api_view, permission_classes, list_route)
from django.core.validators import validate_email
from rest_framework.serializers import ModelSerializer
from law_digest.models import *

class DigestCategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = DigestCategory
		fields = ('id', 'name','description','category_image','author','created_date',
					'modified',)

class DigestTopicSerializer(serializers.ModelSerializer):
	class Meta:
		model = DigestTopic
		fields = ('id','topic_category', 'topic','author','created_date',
					'modified',)

class DigestContentSerializer(serializers.ModelSerializer):
	class Meta:
		model = DigestContent
		fields = ('id','content_category','content','url','content_image','content_image',
					'author','created_date','modified')