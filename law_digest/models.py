from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from django.db import models
from imagekit.models import ProcessedImageField

# Create your models here.
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



class DigestCategory(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    category_image = ProcessedImageField(upload_to='law_digest_category_images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Digest Category"

# Not in use anymore
class DigestTopic(models.Model):
	topic_category = models.ForeignKey(DigestCategory, related_name='topic_category', blank=True, null=True)
	topic = models.CharField(max_length=60)
	author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
	created_date = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)
	def publish(self):
		self.created_date = timezone.now()
		self.save()

	def __unicode__(self):
		return self.topic

	def __str__(self):
		return self.topic

	class Meta:
		verbose_name_plural = "Digest Topics"


class DigestContent(models.Model):
	content_category = models.ForeignKey(DigestCategory, related_name='content_category', blank=True, null=True)
	content = models.TextField(blank=True)
	url = models.CharField(max_length=50, blank=True, null=True)
	content_image = ProcessedImageField(upload_to='digest_content_images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
	author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
	created_date = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)
	def publish(self):
		self.created_date = timezone.now()
		self.save()

	def __unicode__(self):
		return self.content_category.name

	def __str__(self):
		return self.content_category.name

	class Meta:
		verbose_name_plural = "Digest Content"


class ContentServed(models.Model):
	user = models.ForeignKey(User, related_name='served_user', blank=True, null=True)
	content_served = models.ManyToManyField(DigestContent, related_name='content_served', blank=True)

	created_date = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)
	def publish(self):
		self.created_date = timezone.now()
		self.save()

	def __unicode__(self):
		return self.content

	def __str__(self):
		return self.content

	class Meta:
		verbose_name_plural = "Served content"	

