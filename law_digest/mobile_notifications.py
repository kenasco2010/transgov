from django.dispatch import receiver
from django.db.models.signals import post_save
import requests

from gov import credInfo
from rest_api.notifications import get_firebase
from law_digest.models import DigestCategory, DigestTopic, DigestContent

# global variable 
fcm_url = 'http://fcm.googleapis.com/fcm/send'

# Send notification when a category is added.
@receiver(post_save, sender=DigestCategory)
def digest_category_notification(sender, instance, created=False, *args, **kwargs):
	if created:
		category_instance = sender.objects.get(pk=instance.pk)
		url = fcm_url
		headers = {'Authorization': credInfo.FIREBASE_KEY}
		fcm_id = get_firebase()
		payload = { 
	                "data": {
	                  "id":category_instance.id,
	                   "title" : "Hey check out the new category on law digest" + category_instance.name,
	                    "body" : category_instance.description,
	                    "click_action" : "org.transgovgh.app.DrawerActivity",
	                      "type":"category"
	                },
	                "registration_ids": fcm_id
	            }
		r = requests.post(url, headers=headers, json = payload)
	else:
		pass

@receiver(post_save, sender=DigestContent)
def content_nofitication(sender, instance, created=False, *args, **kwargs):
	if created:
		dgc_instance = sender.objects.get(pk=instance.pk)
		url = fcm_url
		headers = {'Authorization': credInfo.FIREBASE_KEY}
		fcm_id = get_firebase()
		payload = { 
	                "data": {
	                  "id":dgc_instance.id,
	                   "title" : "Did you know ?",
	                    "body" : dgc_instance.content,
	                    "click_action" : "org.transgovgh.app.LawDigestContentActivity",
	                    "category_id": dgc_instance.content_category.id,
						"category_name": dgc_instance.content_category.name,
	                    "type":"content"
	                },
	                "registration_ids": fcm_id
	            }
		r = requests.post(url, headers=headers, json = payload)
	else:
		pass