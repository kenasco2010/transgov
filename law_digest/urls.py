from django.conf import settings
from django.conf.urls import patterns, url, include
from django.contrib import admin
admin.autodiscover()

from rest_framework.routers import DefaultRouter

from law_digest.api_views import *
# from law_digest.views import *



router = DefaultRouter()
router.register(r'v1.0/digest-categories', DigestCategoryViewSet)
router.register(r'v1.0/digest-topics', DigestTopicViewSet)
router.register(r'v1.0/digests', DigestContentViewSet) #random digest content
urlpatterns = [
    url(r'^', include(router.urls)),
    # LAW DIGEST URL
    # url(r'^transgov/dashboard/add-digest-category', add_digest_category, name='add_category'),


]
