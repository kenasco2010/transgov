from django import forms
from django.forms import TextInput
from django.contrib.auth.models import *
from django.contrib.auth.models import User
import datetime
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
from law_digest.models import DigestCategory,DigestTopic, DigestContent

class DigestCategoryForm(forms.ModelForm):
    name = forms.CharField(
            label = 'Category Name',
            widget=forms.TextInput(attrs={'class': 'form-control'}),
   )
   
    description = forms.CharField(
            label = 'Description',
            widget = forms.Textarea(attrs={'class': 'form-control', 'rows': 5}),

    )
    category_image = forms.ImageField()

    class Meta:
        model = DigestCategory
        fields = ['name' ,'description', 'category_image']



class DigestTopicForm(forms.ModelForm):
    topic_category = forms.ModelChoiceField(
            queryset = DigestCategory.objects.all(),
            label = 'Select a category',
            widget=forms.Select(attrs={'class': 'form-control'}),
   )
    topic = forms.CharField(
            label = 'Enter a topic',
            widget=forms.TextInput(attrs={'class': 'form-control'}),
   )
    
    class Meta:
        model = DigestTopic
        fields = ['topic_category' ,'topic']


class DigestContentForm(forms.ModelForm):
    
    content = forms.CharField(
            label = 'Add content (maximum 100 characters)',
            widget=forms.Textarea(attrs={'class': "form-control", 'rows': 5, 'placeholder': 'Enter content for the topic.' 
                'Your content should\'nt be more than 100 characters'}),
   )
    url = forms.CharField(
            label = 'Url for the content',
            required=False,
            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter url for the content'}),
   )
    
    class Meta:
        model = DigestContent
        fields = ['content', 'url', 'content_image']

    