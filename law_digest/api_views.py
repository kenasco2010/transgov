from django.utils import timezone
from django.contrib.auth import authenticate, login, logout, get_user_model
from rest_framework import viewsets
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import FileUploadParser


import re
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view

from law_digest.models  import DigestCategory, DigestTopic, ContentServed
from law_digest.serializers import *

# override pagination class
# class StandardResultsSetPagination(PageNumberPagination):
#     page_size = 1
#     page_size_query_param = 'page_size'
#     max_page_size = 1


class DigestCategoryViewSet(viewsets.ModelViewSet):
	model = DigestCategory
	queryset = DigestCategory.objects.all()
	serializer_class = DigestCategorySerializer


	
	@detail_route(methods=('GET',), url_path='content', 
					permission_classes=(IsAuthenticated, ), 
					)
	def get_content_by_category(self, request,  *args, **kwargs):
		category = self.get_object()
		content_id = request.GET.get('content_id')
		
		if content_id:
			digest = DigestContent.objects.filter(content_category=category, id__gt=content_id)
			serialize_digest = DigestContentSerializer(digest, many=True)
			return Response({ "status_code": status.HTTP_200_OK,
							"result": serialize_digest.data
							})
		else:
			digest = DigestContent.objects.filter(content_category=category)
			serialize_digest = DigestContentSerializer(digest, many=True)
			return Response({ "status_code": status.HTTP_200_OK,
							"result": serialize_digest.data
							})

	@detail_route(methods=('GET',), url_path='law_digest_notification_id',
					permission_classes=(IsAuthenticated, ), )
	def get_content_by_notification_id(self, request, pk=None, *args, **kwargs):
		category = self.get_object()
		notification_id = request.GET.get('notification_id')
		
		if notification_id:
			digest = DigestContent.objects.filter(content_category=category, 
				id=notification_id)
			serialize_digest = DigestContentSerializer(digest, many=True)
			return Response({ "status_code": status.HTTP_200_OK,
							"result": serialize_digest.data
							})
		else:
			digest = DigestContent.objects.filter(content_category=category)
			serialize_digest = DigestContentSerializer(digest, many=True)
			return Response({ "status_code": status.HTTP_200_OK,
							"result": serialize_digest.data
							})
	
# Not in use
class DigestTopicViewSet(viewsets.ModelViewSet):
	model = DigestTopic
	queryset = DigestTopic.objects.all()
	serializer_class = DigestTopicSerializer

class DigestContentViewSet(viewsets.ModelViewSet):
	model = DigestContent
	queryset = DigestContent.objects.all()
	serializer_class = DigestContentSerializer



	
		