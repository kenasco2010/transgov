from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from rest_api.models import ProjectCase
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class FaultVote(models.Model):
	fault = models.ForeignKey(ProjectCase, related_name='votes', blank=True, null=True)
	user = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE, related_name='user_votes')
	date_voted = models.DateTimeField(default=timezone.now)
	modified = models.DateTimeField(auto_now=True)

	def publish(self):
		self.date_voted = timezone.now()
		self.save()

	def __unicode__(self):
		return self.fault.case_name

	def __str__(self):
		return self.fault.case_name

	class Meta:
		verbose_name_plural = "Fault Votes"

	# @property
	# def vote_user(self):
	# 	userId = self.author.id
	# 	vote_user = ProjectCase.object.get(author=userId)
	# 	if vote_user:
	# 		return vote_user
	# 	else:
	# 		return False