from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^$', views.homePage, name='index'),
    # url(r'^welcome/$', views.front_view, name='front_view'),
    # url(r'^about/$', views.about, name='aboutPage'),
    # url(r'^listings/$', views.listings, name='listingsPage'),
    # url(r'^detail/$', views.detail, name='detailPage'), #uses id of listing card.
    # url(r'^blog/$', views.blog, name='blog'),
    # url(r'^contact-us/$', views.contact, name='contact'),
    # url(r'^our-partners/$', views.partner_info, name='partners'),

]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
