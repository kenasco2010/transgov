from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from imagekit.models import ProcessedImageField
from rest_api.models import CaseCategory, convert_category_to_tuple
from multiselectfield import MultiSelectField
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



# Create your models here.
class CompanyProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=200, blank=True)
    # not using company category anymore
    company_category = models.CharField(max_length=200, blank=True)
    company_type = models.ForeignKey(CaseCategory, related_name='company_type', 
        blank=True, null=True)
    company_interest= MultiSelectField(choices=convert_category_to_tuple())
    description = models.TextField()
    address = models.CharField(max_length=200, blank=True)
    location = models.CharField(max_length=200, blank=True)
    phone_number = models.CharField(max_length=200, blank=True)
    company_email = models.CharField(max_length=100, blank=True)
    company_logo = ProcessedImageField(upload_to='company_profile_logo',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.company_name

    def __str__(self):
        return self.company_name

    class Meta:
            verbose_name_plural = "Company Profile"
