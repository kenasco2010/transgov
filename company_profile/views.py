from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.decorators import *
from django.http import HttpResponse
from django.utils import timezone
from django.shortcuts import redirect, render, get_object_or_404

from .forms import CompanyProfileForm
from .models import CompanyProfile

from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives




# Create your views here.
@login_required()
def add_company_profile(request):
    if request.user.is_superuser:
        if request.method == "POST":
            company_profile_form = CompanyProfileForm(request.POST, request.FILES)
            if company_profile_form.is_valid():
                company_profile_form = company_profile_form.save(commit=False)
                company_profile_form.published_date = timezone.now()
                request.user.is_staff = True
                company_profile_form.save()
                return redirect('transgovDashboard')
        else:
            company_profile_form = CompanyProfileForm()
        return render (request, 'company_profile/add_company_profile.html', {'form': company_profile_form})
    else:
        return render(request, 'project/not_attached_to_company.html')

@login_required()
def edit_company_profile(request, pk):
    comp_prof = get_object_or_404(CompanyProfile, pk=pk)
    if request.method == "POST":
        company_profile_form = CompanyProfileForm(request.POST, request.FILES, instance=comp_prof)
        if company_profile_form.is_valid():
            company_profile_form = company_profile_form.save(commit=False)
            company_profile_form.user = request.user
            company_profile_form.published_date = timezone.now()
            company_profile_form.save()
            return redirect('transgovDashboard')
    else:
        company_profile_form = CompanyProfileForm(instance=comp_prof)
    return render (request, 'company_profile/edit_company_profile.html', {'form': company_profile_form})

@login_required()
def view_company_profile_dashbaord(request):
    try:
        if request.user.is_authenticated():
            user = request.user
            get_company_profile = CompanyProfile.objects.get(user=user)
            # if get_company_profile.company_name:
            #   return {"company_name": get_company_profile.company_name}
            # else:
            #     return {"company_name": "/create-company-profile/"}
            context = {
            'company_profile': get_company_profile
            }
            return render(request, 'company_profile/view_company_profile.html', context)
    except CompanyProfile.DoesNotExist:
        return render(request, 'company_profile/prompt_to_add_company_profile.html')


# add users to company
@login_required()
def add_users_to_company_dashboard(request, *args, **kwargs):
    comp_profile = CompanyProfile.objects.get(user=request.user)
    if request.method == 'POST':
        email = request.POST.get('email')
        htmly = get_template('sendEmails/invite_user_to_dashboard_email.html')
        plaintext = get_template('sendEmails/invite_user_to_dashboard_email.txt')
        d = Context({ 'email': email,
                      'company_name': comp_profile,
                      'case_name': "instancecase_name"
                      })
        email_subject = 'Invitation to view dashboard'
        to=email
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(email_subject, text_content,
                                     settings.EMAIL_HOST_USER, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    return render(request, 'company_profile/add_users_to_company.html')


@login_required()
def list_of_companies(request):
    company_list = CompanyProfile.objects.all().order_by('-id')
    context = {
    'company_list': company_list
    }
    return render(request, 'company_profile/list_of_companies.html', context)


# def send_email_invite(request):
    
