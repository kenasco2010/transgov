from django import forms
from django.forms import TextInput, ModelForm
from .models import CompanyProfile
from django.contrib.auth.models import User
from django.contrib.auth.models import *
from rest_api.models import CaseCategory


class CompanyProfileForm(forms.ModelForm):
    company_name = forms.CharField(
        label = "Company Name",
        widget=forms.TextInput(attrs={'class': "form-control company-form", "placeholder":"Your company full name"}),
        )
    company_type = forms.ModelChoiceField(
        queryset = CaseCategory.objects.all(),
        label = 'Comapany category',
        required = False,
        widget=forms.Select(attrs={'class': "form-control company-form"}),
        )
    description = forms.CharField(
        label = "Company description",
        required=False,
        widget=forms.Textarea(attrs={'class': "form-control company-form", 'rows': 4}),
        )
    address = forms.CharField(
        label = "Company address",
        required=False,
        widget=forms.Textarea(attrs={'class': "form-control company-form", 'rows': 4,
                                    "placeholder":"Your company address"}),
        )
    location = forms.CharField(
        label = "Location",
        required=False,
        widget=forms.TextInput(attrs={'class': "form-control company-form",
                                    "placeholder":"Location of your company"}),
        )
    phone_number = forms.CharField(
        label = "Company phone number",
        required=False,
        widget=forms.TextInput(attrs={'class': "form-control company-form",
                                "placeholder":"Company phone number"}),
        )
    company_email = forms.CharField(
        label = "Company email",
        required=False,
        widget=forms.TextInput(attrs={'class': "form-control company-form",
                                    "placeholder":"Company email" }),
        )

    user = forms.ModelChoiceField(
        queryset = User.objects.all(),
        label = "Company user",
        required = False,
        widget = forms.Select(attrs={"class": "form-control"})
        )

    class Meta:
        model = CompanyProfile

        fields = ['company_name', 'company_interest', 'description', 'address',
                'location', 'phone_number', 'company_email', 'company_logo']


# class SendEmailInviteForm(forms.ModelForm):
#     email_invite = forms.CharField(
#         label = "Email",
#         max_length=50,
#         widget = forms.Select(attrs={"class": "form-control"})
#         )

#     class Meta:
#         fields =['email_invite']