from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings

from imagekit.models import ProcessedImageField

from django.db import models
from rest_api.models import *
from company_profile.models import *

# Create your models here.


# This is the model for case category
class FieldWorker(models.Model):
    full_name = models.CharField(max_length=50, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    company = models.ForeignKey(CompanyProfile, related_name='field_worker_company', blank=True, null=True)
    suburb = models.ForeignKey(Suburb, related_name="field_worker_suburb",  blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True)
    field_worker_image = ProcessedImageField(upload_to='field_worker_image',
                                        format='JPEG',
                                        options={'quality': 20},
                                        null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.full_name

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name_plural = "Field workers"