from django import forms
from django.forms import TextInput
from .models import CompanyProfile
from django.contrib.auth.models import User
from django.contrib.auth.models import *
from company_profile.models import CompanyProfile
from rest_api.models import Suburb, CaseCategory
from field_worker.models import FieldWorker


class FieldWorkerForm(forms.ModelForm):
    full_name = forms.CharField(
        label = "Your full name",
        widget=forms.TextInput(attrs={'class': "form-control", 
            "placeholder":"Please enter your full name"}),
        )
    company = forms.ModelChoiceField(
        queryset = CompanyProfile.objects.all(),
        label = 'Company',
        required = False,
        widget=forms.Select(attrs={'class': "form-control"}),
        )
    suburb = forms.ModelChoiceField(
        queryset = Suburb.objects.all(),
        label = 'Choose your suburb',
        required = False,
        widget=forms.Select(attrs={'class': "form-control"}),
        )
    phone_number = forms.CharField(
        label = "Add phone number",
        required=False,
        widget=forms.TextInput(attrs={'class': "form-control", 
            "placeholder":"Please enter your phone number"}),
        )
    # user = forms.ModelChoiceField(
    #     queryset = User.objects.all(),
    #     label = "Choose a user this field worker belongs to",
    #     required = False,
    #     widget = forms.Select(attrs={"class": "form-control"})
    #     )

    class Meta:
        model = FieldWorker
        fields = ['full_name', 'company', 'suburb', 'phone_number',
                'field_worker_image']
