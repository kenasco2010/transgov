from __future__ import unicode_literals

from django.apps import AppConfig


class FieldWorkerConfig(AppConfig):
    name = 'field_worker'
