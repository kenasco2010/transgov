from django.shortcuts import redirect, render, get_object_or_404
# Create your views here.

from django.views.generic.base import View
from django.contrib.auth.decorators import *
from django.http import HttpResponse
from django.utils import timezone
from django.core.paginator import Paginator
from django.dispatch import receiver
from django.db.models.signals import post_save

# send sms with mnotify
import urllib2
import urllib
import requests

from .forms import FieldWorkerForm
from .models import FieldWorker
from company_profile.models import CompanyProfile
from assign_faults.views import list_of_assigned_faults_to_fieldworker
from assign_faults.models import AssignFault
from rest_api.models import ProjectCase


# Twilio
from twilio.rest import Client
from gov.credInfo import *


# Create your views here.
@login_required()
def add_field_worker_profile(request):
    if request.method == "POST":
        field_worker_form = FieldWorkerForm(request.POST, request.FILES)
        if field_worker_form.is_valid():
            field_worker_form = field_worker_form.save(commit=False)
            field_worker_form.published_date = timezone.now()
            field_worker_form.save()
            return redirect('field_workers')
    else:
        field_worker_form = FieldWorkerForm()
    return render (request, 'field_worker_profile/add_field_worker.html', {'form': field_worker_form})


@login_required()
def edit_field_worker_profile(request, pk):
    field_worker = get_object_or_404(FieldWorker, pk=pk)
    if request.method == "POST":
        field_worker_form = FieldWorkerForm(request.POST, request.FILES, instance = field_worker)
        if field_worker_form.is_valid():
            field_worker_form = field_worker_form.save(commit=False)
            field_worker_form.published_date = timezone.now()
            field_worker_form.save()
            return redirect('field_workers')
    else:
        field_worker_form = FieldWorkerForm(instance=field_worker)
    return render (request, 'field_worker_profile/edit_field_worker.html', {'form': field_worker_form})


@login_required()
def field_worker_list_dashboard(request): # list of field workers on it's dashboard
    company_profile = CompanyProfile.objects.get(user=request.user)
    field_worker_list = FieldWorker.objects.all().order_by('-id'
        ).filter(company__company_type=company_profile.company_type)
    paginator = Paginator(field_worker_list, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        field_worker_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        field_worker_list = paginator.page(paginator.num_pages)
    # faults_assigned_to_each_field_worker = list_of_assigned_faults_to_fieldworker(request)
    context = {
       'field_workers': field_worker_list
    }
    return render(request, 'field_worker_profile/field_worker_list.html', context)


# load assigned faults in a modal
def load_assigned_faults_modal(request):
    field_worker_id = request.POST.get('field_worker_id')
    get_faults = AssignFault.objects.filter(field_worker=field_worker_id)
    
    context = {
       'field_workers': get_faults
    }
    return render(request, 'field_worker_profile/remodal.html', context)
   

# field worker profile detail
@login_required()
def field_worker_profile(request, pk):
    profile = get_object_or_404(FieldWorker, pk=pk)
    assigned_faults = list_of_assigned_faults_to_fieldworker(request, pk) # function coming from assigned faults views
    context = {
        'profile': profile,
        'assigned_faults': assigned_faults
    }
    return render(request, 'field_worker_profile/view_field_worker_profile.html', context)

# remove field worker from the system
def delete_field_worker(request, pk):
    field_worker = get_object_or_404(FieldWorker, pk=pk)
    field_worker.delete()
    return redirect('field_workers')


# display field worker on faults details page. 
@login_required()
def display_field_worker_on_fault_detail(request):
    company_profile = CompanyProfile.objects.get(user=request.user)
    field_worker_list = FieldWorker.objects.all().order_by('-id'
        ).filter(company__company_type=company_profile.company_type)
    context = {
       'field_workers': field_worker_list
    }
    return field_worker_list


# Send Sms to field worker when Fault is assigned to him
@receiver(post_save, sender=AssignFault)
def send_sms_to_field_worker(sender, instance, created=False, *args, **kwargs):
    url = "https://apps.mnotify.net/smsapi"
    if created:
        obj = sender.objects.get(pk=instance.pk)
        field_worker_phone_number = obj.field_worker.phone_number
        field_worker_name = obj.field_worker.full_name
        fault_number = obj.community_fault.case_number
        fault_name = obj.community_fault.case_name
        fault_location = obj.community_fault.map_land_mark_name
        message_body = '''A fault reported as {}, with unique fault number \n
{}, has been assigned to you {}. The location of the fault is around {}. \n
Please contact admin for more information. '''.format(
            fault_name, fault_number, field_worker_name, fault_location)
        api_key = 'rTFOkZ8lz4cmBLbCVCgDjSpkf'
        sender_id = 'TransGov'
        querystring = {
        "msg":message_body,
        "to":field_worker_phone_number,
        "sender_id":sender_id,
        "key":api_key
        }
        response = requests.request("GET", url,  params=urllib.urlencode(querystring))
        
# # send sms when the SMS button is clicked on front end
# def send_sms_only(request):
#     url = "https://apps.mnotify.net/smsapi"
#     if request.method == "POST":
#         fault_id = request.POST.get('fault_id')
#         field_worker = request.POST.get('field_worker_id')
#         fault_id = request.POST.get('fault_id')
#         field_worker = FieldWorker.objects.get(id=field_worker)
#         fault = ProjectCase.objects.get(id=fault_id)

#         field_worker_phone_number = field_worker.phone_number
#         field_worker_name = field_worker.full_name
#         fault_number = fault.case_number
#         fault_name = fault.case_name
#         fault_location = fault.map_land_mark_name
#         message_body = '''A fault reported as {}, with unique fault number {}, has been assigned to you {}. The location of the fault is around {}. Please contact admin for more information. '''.format(
#             fault_name, fault_number, field_worker_name, fault_location)
#         querystring = {
#         "msg":message_body,
#         "to":field_worker_phone_number,
#         "sender_id":sender_id,
#         "key":api_key
#         }
#         response = requests.request("GET", url,  params=urllib.urlencode(querystring))
        
    
    

