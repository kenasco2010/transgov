"""gov URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, patterns, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from project.models import *
from project.views import *
from push_notification.views import send_push_notification
from company_profile.views import *
from field_worker.views import *
from assign_faults.views import *
from law_digest.views import *

# from django.conf.urls import handler500, handler404
admin.site.site_header = 'Transgov'

handler500 = 'project.views.handler500'
handler404 = 'project.views.handler404'

urlpatterns = [

    # ADMIN DASGBOARD URLS
    url(r'^admin/', include(admin.site.urls)),

    # api url
    url(r'^api/', include('rest_api.urls', namespace='api')),

    # include transgov partners url
    url(r'^api/', include('transgov_partners.urls', namespace='api')),

    # law digest api url include
    url(r'^api/', include('law_digest.urls', namespace='api')),
    
    # redactor wysiwyg editor
    url(r'^redactor/', include('redactor.urls')),
    url(r'', include('landingPage.urls')),
    url(r'', include('company_profile.urls')),
    # url(r'', include('event.urls')),
    url(r'^accounts/', include('all_auth.urls')),
    url(r'^inapi/', include('api.urls')),

    # url for export of data to csv
    # url(r'^export/csv/ecg$', export_ecg_report_csv, name='export_ecg_csv'),
    url(r'^export/faults-data/csv/$', export_fault_as_csv, name='export_faults_csv'),

    # DASHBOARD URLS
    url(r'^transgov/dashboard/add-project-information$', add_project_info, name='addProjectInfo'),
    url(r'^transgov/dashboard/$', transgov_dashboard, name='transgovDashboard'),
    url(r'^transgov/dashboard/project-info/(?P<pk>\d+)/edit$', edit_project_info, name='editProjectInfo'),
    url(r'^transgov/dashboard/project-details/(?P<pk>\d+)$', project_details_on_dashboard, name='transgovAdminProjectDetails'),
    url(r'^transgov/dashboard/list-of-projects$', project_list_on_dashboard, name='viewProjectsonDashboard'),
    url(r'^transgov/dashboard/projects-for-periodic-update$', periodic_update_project_list, name='periodicProjectUpdateList'),
    url(r'^transgov/dashboard/(?P<pk>\d+)/add-field-visit-project-update$', add_periodic_project_update, name='addPeriodicProjectUpdate'),

    url(r'^transgov/dashboard/select-prj-to-add-images$', select_proj_add_image, name='clickToAddImages'),
    url(r'^transgov/dashboard/project/(?P<pk>\d+)/add-images$', add_images_to_project, name='addImagesToProject'),

    # Faults report dashboard
    url(r'^transgov/dashboard/list-of-all-cases', all_diy_cases, name='viewAllCasesOnDashboard'),
    url(r'^transgov/dashboard/list-of-pending-cases', pending_cases, name='viewPendingOnDashboard'),
    url(r'^transgov/dashboard/list-of-onging-fixes', fixing_cases, name='viewOngoingFixesOnDashboard'),
    url(r'^transgov/dashboard/list-of-fixed-cases', fixed_cases, name='viewFixedOnDashboard'),
    url(r'^transgov/dashboard/case/(?P<pk>\d+)/details/', case_details, name='caseDetailsOnDashboard'),
    url(r'^transgov/dashboard/project-case/(?P<pk>\d+)/edit$', edit_project_case, name='editProjectCase'),
    url(r'^transgov/dashboard/project-case/(?P<pk>\d+)/delete$', delete_case, name='delete_case'),
    url(r'^transgov/dashboard/search-fault', fault_search, name='search_fault'),

    # News
    url(r'^transgov/dashboard/add-news/', add_news, name='add_news'),
    url(r'^transgov/dashboard/edit-news/(?P<pk>\d+)/edit$', edit_news, name='edit_news'),
    url(r'^transgov/dashboard/news-list', news_list, name='news_list'),
    url(r'^transgov/dashboard/news/(?P<pk>\d+)/details', dashboard_news_details, name='dashboard_news_detail'),
    url(r'^transgov/dashboard/news(?P<pk>\d+)/delete$', delete_news, name='delete_news'),


    # Company profiles url
    url(r'^transgov/dashboard/create-company-profile/', add_company_profile, name='create_company_profile'),
    url(r'^transgov/dashboard/company-profile/view', view_company_profile_dashbaord, name='view_company_profile'),
    url(r'^transgov/dashboard/company-profile/(?P<pk>\d+)/edit$', edit_company_profile, name='edit_comp_profile'),
    url(r'^transgov/dashboard/invite-users/$', add_users_to_company_dashboard, name='invite_users'),
    url(r'^transgov/dashboard/companies-registered/$', list_of_companies, name='list_of_companies'),



    # fault reported status url
    url(r'^transgov/dashboard/faults/status', specificfaultreportstatus, name='specific_faults_status'),
    url(r'^transgov/dashboard/all-faults/status', allfaultreportstatus, name='all_faults_status'),


    # field worker urls
    url(r'^transgov/dashboard/add-field-worker', add_field_worker_profile, name='add_field_worker'),
    url(r'^transgov/dashboard/field-workers', field_worker_list_dashboard, name='field_workers'),
    url(r'^transgov/dashboard/field-worker/(?P<pk>\d+)/details', field_worker_profile, name='view_field_worker_detail'),
    url(r'^transgov/dashboard/field-worker/(?P<pk>\d+)/edit', edit_field_worker_profile, name='edit_field_worker_profile'),
    url(r'^transgov/dashboard/field-worker/(?P<pk>\d+)/delete', delete_field_worker, name='delete_field_worker'),
    
    # url(r'^transgov/dashboard/send-sms-to-field-worker', send_sms_only, name='send_sms_only'),

    # assign fault
    url(r'^transgov/dashboard/list-of-assigned-faults', list_of_assigned_faults, name='assigned_faults_list'),
    url(r'^transgov/dashboard/faultassigned/remove/', delete_fault_assignment, name='delete_assign_fault'),
    url(r'^transgov/dashboard/modal-fault-assigned/', load_assigned_faults_modal, name='load_assigned_faults_modal'),
    url(r'^transgov/dashboard/admin-insight/', admin_insight, name='admin_insights'),


    #PUBLIC VIEW URLS
    url(r'^projects$', public_view_projects, name='publicViewProjects'),
    url(r'^project-details/(?P<pk>\d+)$', project_details, name='projectDetails'),

    # url for project likes
    url(r'^likes/$', project_likes, name='project_likes'),
    url(r'^likes-on-listing-page/$', listing_page_project_likes, name='listing_page_project_likes'),

    # subscribe on project details page.
    url(r'^subscribe/$', project_details, name='subscribe'),

    # IVR url
    url(r'^transgov/dashboard/add-ivr-number$', add_IVR_number, name='addIVRContact'),
    url(r'^transgov/dashboard/ivr-numbers$', ivr_list_on_dashboard, name='viewIVRContacts'),

    # Bulk sms
    url(r'^transgov/dashboard/add-bulksms-number$', add_bulkSMS_number, name='addBulkSMSNumber'),
    url(r'^transgov/dashboard/bulksms-numbers$', bulkSMS_list_on_dashboard, name='viewBulkSMSContacts'),

    # URLS FOR STATIC PAGES
    url(r'^about-transgov$', about_transgov, name='aboutTransgov'),
    url(r'^$', index_page, name='homePage'),
    url(r'^partners$', partners, name='partners'),
    url(r'^contact$', contact, name='contact'),
    url(r'^privacy_policy$', privacy_policy, name='privacy_policy'),
    url(r'^notfound$', notfound, name='notfound'),
    url(r'^news-list$', public_news_list, name='public_news_list'),
    url(r'^news/(?P<pk>\d+)/details', news_details_for_public, name='news_detail'),
    # url(r'^project/(?P<pk>\d+)/comment/$', project_details, name='add_comment_to_project'),

    # subscribe
    url(r'^project-details/(?P<pk>\d+)/delete$', delete_subscriber, name='deleteSubscriber'),

    # Search Results page
    url(r'^search_results$', search_results, name='searchResult'),

    # Rest Framework
    url(r'^docs/', include('rest_framework_swagger.urls')),

    # Urls for events
    url(r'^transgov/dashboard/event/add-event', add_event, name='addEvent'),
    url(r'^transgov/dashboard/event/list-of-events', list_of_events_dashboard, name='eventsList'),
    url(r'^transgov/dashboard/event/detail/(?P<pk>\d+)', event_details_on_dashboard, name='eventDetail'),
    url(r'^transgov/dashboard/event/(?P<pk>\d+)/edit', edit_event, name='editEvent'),
    url(r'^events/', public_view_event, name='publicListOfEvents'),
    url(r'^event/(?P<pk>\d+)/detail', event_details_for_public, name='publicEventDetail'),
    url(r'^add-event-image', add_event_images, name='addEventImages'),
    url(r'^transgov/dashboard/event/(?P<pk>\d+)/delete', delete_event, name='delete_event'),


    # project updates urls
    # url(r'^project-details/(?P<pk>\d+)/project_update$', show_project_update_to_public, name='publicViewProjectUpdate')
    # url for likes
    # url(r'^like/$', public_view_projects, name='like'),


    # push notifications
    url(r'^transgov/dashboard/push-notification', send_push_notification, name='push_notification'),

    # LAW DIGEST URL
    url(r'^transgov/dashboard/add-digest-category', add_digest_category, name='add_category'),
    url(r'^transgov/dashboard/digest-category/(?P<pk>\d+)/edit', edit_digest_category, name='edit_digest_category'),
    url(r'^transgov/dashboard/digest-category/(?P<pk>\d+)/delete', delete_digest_category, name='delete_digest_category'),
    url(r'^transgov/dashboard/category-list', digest_category_list, name='category_list'),
    # url(r'^transgov/dashboard/add-digest-topics', add_topic, name='add_topic'),
    # url(r'^transgov/dashboard/digest-topic-list', topic_list, name='digest_topic_list'),
    url(r'^transgov/dashboard/select-category-to-add-content', select_category_list_to_add_content, name='category_list_to_add_content'),
    url(r'^transgov/dashboard/cat_id-(?P<pk>\d+)/add-digest-content', add_content, name='add_digest_content'),
    url(r'^transgov/dashboard/cat_id-(?P<pk>\d+)/contents', view_content, name='view_content'),
    url(r'^transgov/dashboard/content/(?P<pk>\d+)/edit', edit_content, name='edit_content'),
    url(r'^transgov/dashboard/content/(?P<pk>\d+)/delete', delete_digest_content, name='delete_content'),


    # Help page url
    url(r'^transgov/dashboard/help', help_page, name='help_page'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# if settings.DEBUG:
#     urlpatterns += patterns('',
#         url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#             'document_root': settings.MEDIA_ROOT,
#             }),
#         )
