# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-11-21 05:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import imagekit.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TransGovPartners',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('partner_name', models.CharField(blank=True, max_length=200)),
                ('partner_description', models.TextField()),
                ('partner_logo', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='transgov_partner_logo')),
                ('partner_website', models.CharField(blank=True, max_length=200)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'TransGov Partners',
            },
        ),
    ]
