from django.conf import settings
from django.conf.urls import patterns, url, include
from django.contrib import admin
admin.autodiscover()

from rest_framework.routers import DefaultRouter

from transgov_partners.views import *
# from rest_api.urls import *


router = DefaultRouter()
router.register(r'v1.0/transgov-partners', TransGovPartnersViewSet)
urlpatterns = [
    url(r'^', include(router.urls)),

]
