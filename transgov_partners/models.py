from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from imagekit.models import ProcessedImageField

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class TransGovPartners(models.Model):
    partner_name = models.CharField(max_length=200, blank=True)
    partner_description = models.TextField()
    partner_logo = ProcessedImageField(upload_to='transgov_partner_logo',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    partner_website = models.CharField(max_length=200, blank=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.partner_name

    def __str__(self):
        return self.partner_name

    class Meta:
            verbose_name_plural = "TransGov Partners"



