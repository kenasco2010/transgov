from django.shortcuts import render
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.response import Response

from transgov_partners.serializers import *
from transgov_partners.models import TransGovPartners


class TransGovPartnersViewSet(viewsets.ModelViewSet):
	model = TransGovPartners
	serializer_class = TransGovPartnersSerializer
	queryset = TransGovPartners.objects.all()