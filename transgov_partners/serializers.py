from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from transgov_partners.models import TransGovPartners


class TransGovPartnersSerializer(serializers.ModelSerializer):

	class Meta:
		model = TransGovPartners
		fields = ('id', 'partner_name', 'partner_description', 'partner_logo', 'partner_website')

