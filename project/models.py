from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.utils import timezone
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from decimal import Decimal
from datetime import date
# from django_random_queryset import RandomManager
from rest_framework.authtoken.models import Token

# imports for image compress
from boto.s3.connection import S3Connection
from cStringIO import StringIO
from PIL import Image as mypil
import PIL
import os
# from redactor.fields import RedactorField
from geoposition.fields import GeopositionField
from project.choices import *

# Project model .
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create auth token for new users
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class ProjectInfo(models.Model):
    # objects = RandomManager()
    project_name = models.CharField(max_length=400)
    project_description = models.TextField()
    amount_allocated = models.CharField(max_length=200, 
        null=True, blank=True)
    estimated_budget_GOG = models.CharField(default=None, 
        max_length=200, null=True, blank=True)
    estimated_budget_IGF = models.CharField(max_length=200, 
        null=True, blank=True)
    estimated_budget_Donor = models.CharField(max_length=200, 
        null=True, blank=True)
    actual_Budget_Spend = models.CharField(max_length=200, 
        null=True, blank=True)
    project_contact_person = models.CharField(max_length=200, 
        null=True, blank=True)
    project_contractor = models.CharField(max_length=200, 
        null=True, blank=True)
    project_status = models.CharField(choices=PROJECT_STATUS, 
        max_length=20, null=True, blank=True)
    project_category = models.CharField(choices=CATEGORY, 
        max_length=100, null=True, blank=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  
        on_delete=models.CASCADE)
    # lead_implementation_Agency = models.ForeignKey(LeadImplementationAgency, blank=True, null=True, related_name="lead_implementation_agency", max_length=900)
    start_date = models.DateField(("Date"), 
        default=date.today, null=True, blank=True)
    end_date = models.DateField(("Date"), 
        default=date.today, null=True, blank=True)
    completeness = models.CharField(max_length=10, 
        null=True, blank=True)
    # time_created = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name="likes", blank=True)
    count_likes = models.IntegerField(default=0)
    like_success = models.BooleanField(default=False)
    project_location = models.CharField(max_length=100, 
        null=True, blank=True)
    regions = models.CharField(choices=REGIONS, 
        max_length=150, null=True, blank=True)
    sub_metro = models.CharField(choices=SUBMETROS, 
        max_length=150, null=True, blank=True)
    #########################################################
    # For location package
    position = GeopositionField(null=True, blank=True)

    # #####################################################
    project_image = ProcessedImageField(upload_to='images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    source = models.CharField(max_length=150, null=True, blank=True)
    # Project_files = models.FileField(max_length=200, null=True, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.project_name

    def __str__(self):
        return self.project_name

    class Meta:
        verbose_name_plural = "Project Information"

    # def image_tag(self):
    #     return mark_safe('<img src="/images/%s" width="150" height="150" />' % (self.image))
    # display_image.allow_tags = True
    # image_tag.short_description = 'Image'

    #       def display_image(self):
    #     return mark_safe('<img style src="%s" width="100" height="100"/> ') % self.get_200_url()
    # display_image.allow_tags = True
    # display_image.short_description = "Flip Image"

#
# AWS_KEY = os.environ.get('AWS_ACCESS_KEY_ID')
# AWS_SECRET = os.environ.get('AWS_SECRET_ACCESS_KEY')
#
# def start_compressing(theimage):
#     conn = S3Connection(AWS_KEY, AWS_SECRET)
#     bucket = conn.get_bucket('transgovgh-production')
#
#     pic = bucket.get_key(theimage)
#
#     # try:
#     if pic.size > 100000:
#         input_file = StringIO(pic.get_contents_as_string())
#
#         img = mypil.open(input_file)
#
#         basewidth = 500
#         print ("basewidth")
#         wpercent = (basewidth / float(img.size[0]))
#
#         hsize = int((float(img.size[1]) * float(wpercent)))
#         img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
#
#         tmp = StringIO()
#         img.save(tmp, 'JPEG', optimize=True, quality=75)
#         tmp.seek(0)
#         print ("quality")
#         output_data = tmp.getvalue()
#
#         headers = dict()
#         headers['Content-Type'] = 'image/jpeg'
#         headers['Content-Length'] = str(len(output_data))
#         pic.set_contents_from_string(output_data, headers=headers, policy='public-read')
#
#         tmp.close()
#         input_file.close()
#         print 'compression done v1'
#         return True
#
#     return 'File Size Less than 100KB'
#     # except:
#     # return 'Unable to do Compression'
#
# @receiver(post_save, sender=ProjectInfo)
# def compress_image(sender, instance=None, created=False, **kwargs):
#     if created:
#         if instance.project_image:
#             tostrip = instance.project_image
#             start_compressing(tostrip)



class ProjectComment(models.Model):
    project = models.ForeignKey(ProjectInfo, related_name='comments')
    commenter_name = models.CharField(max_length=200, blank=True, null=True)
    comment = models.TextField(max_length=3000)
    published_date = models.DateTimeField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)



    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.project.project_name

    def __str__(self):
        return self.project.project_name

    class Meta:
        ordering = ('-timestamp','-id')
        verbose_name_plural = 'Project Comments'

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)

class ProjectImage(models.Model):
    project = models.ForeignKey(ProjectInfo, related_name='projectImages')
    additional_project_images = ProcessedImageField(upload_to='images',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.project.project_name

    def __str__(self):
        return self.project.project_name

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.additional_project_images.url))
    image_tag.allow_tags = True
    image_tag.short_description = 'Image'

# @receiver(post_save, sender=ProjectImage)
# def compress_image(sender, instance=None, created=False, **kwargs):
#     if created:
#         if instance.project_image:
#             tostrip = instance.project_image
#             start_compressing(tostrip)


class ProjectSubscription(models.Model):
    # project = models.ForeignKey(ProjectInfo, related_name='project', blank=True, null=True)
    projectId = models.CharField(blank=True, null=True, max_length=150)
    project_name = models.CharField(blank=True, null=True, max_length=250)
    user_email = models.CharField(blank=True, null=True, max_length=150)
    userName = models.CharField(blank=True, null=True, max_length=150)
    user_fullName = models.CharField(blank=True, null=True, max_length=150)
    current_url = models.CharField(max_length=500)
    phone_number = models.CharField(blank=True, null=True, max_length=150)
    subscribe_status = models.CharField(blank=True, null=True, max_length=150)
    created_date = models.DateTimeField(auto_now_add=True)
    published_date = models.DateTimeField(blank=True, null=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return unicode(self.project_name)

    def __str__(self):
        return unicode(self.project_name)

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class PeriodicProjectUpdate(models.Model):
    project_id = models.CharField(blank=True, null=True, max_length=150)
    project_name = models.CharField(blank=True, null=True, max_length=150)
    percentage_complete = models.CharField(blank=True, null=True, max_length=150)
    field_visit_date = models.DateTimeField(blank=True, null=True)
    project_status = models.CharField(choices=PERIODIC_PROJECT_STATUS, max_length=20, null=True, blank=True)
    comment = models.TextField()
    project_update_images = ProcessedImageField(upload_to='images',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    published_date = models.DateTimeField(blank=True, null=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.project_name

    def __str__(self):
        return self.project_name

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class Event(models.Model):
    event_name = models.CharField(max_length=300)
    event_description = models.TextField()
    start_date = models.DateField(("Date"), default=date.today, null=True, blank=True)
    event_time = models.CharField(max_length=100, null=True, blank=True)
    venue = models.CharField(max_length=200, null=True, blank=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    region = models.CharField(choices=REGIONS, max_length=150, null=True, blank=True)
    sub_metro = models.CharField(choices=SUBMETROS, max_length=150, null=True, blank=True)
    #########################################################
    # For location package
    position = GeopositionField(null=True, blank=True)

    # #####################################################
    event_image = ProcessedImageField(upload_to='images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.event_name

    def __str__(self):
        return self.event_name

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class EventImage(models.Model):
    event = models.ForeignKey(Event, related_name='event_images')
    event_image = ProcessedImageField(upload_to='images',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.event.event_name

    def __str__(self):
        return self.event.event_name

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class BulkSMS(models.Model):
    phone_numbers = models.CharField(max_length=300)
    published_date = models.DateTimeField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    # author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.phone_numbers

    def __str__(self):
        return self.phone_numbers

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class IVR(models.Model):
    phone_number = models.CharField(max_length=300)
    published_date = models.DateTimeField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.phone_number

    def __str__(self):
        return self.phone_number

    def get_elapsed_time(self):
        return naturaltime(self.timestamp)


class News(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField()
    written_by = models.CharField(max_length=300)
    published_date = models.DateField(blank=True, null=True)
    news_image = ProcessedImageField(upload_to='news-images',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
    created_date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)


    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
            verbose_name_plural = "News"



