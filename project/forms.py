from django import forms
from django.forms import TextInput
from .models import ProjectInfo, ProjectComment, ProjectImage, \
                    PeriodicProjectUpdate,Event, BulkSMS, IVR, \
                    EventImage, ProjectSubscription, News
from rest_api.models import ProjectCase, CaseCategory
# from django.forms import ModelForm
from django.contrib.auth.models import *
from project.choices import *
from rest_api.choices import *
# from django.conf import settings
from django.contrib.auth.models import User
# from django.template import RequestContext
# from django.shortcuts import render, redirect
import datetime
# from datetimewidget.widgets import DateTimeWidget
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
# redactor wysiwyg editor
# from redactor.widgets import RedactorEditor


class ProjectInfoForm(forms.ModelForm):
    project_name = forms.CharField(
            label = 'Name of project',
            widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    project_category = forms.ChoiceField(
        choices=CATEGORY,
        initial='Approved',
        label='Project category',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    project_description = forms.CharField(
            label = "project description",
        #     widgets = {
        #    'project_description': RedactorEditor(),
        # }
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 5, 'cols': 20, 'id':'project_description'}),

)
    project_contractor = forms.CharField(
     label = 'contractor for the project',
            widget=forms.TextInput(attrs={'class': "form-control"}),
   )

    amount_allocated = forms.CharField(
            label = 'Amount allocated to project',
            initial='0',
            error_messages={
            'required': 'This field is required',
            'min_length': 'The name of the course should not exceed %(show_value)',
        },
            widget=forms.TextInput(attrs={'class': "form-control"}),
    )
    estimated_budget_GOG = forms.CharField(
            max_length =200,
            label = 'Estimated budget of GOG',
            initial='0',
            error_messages = {
                'min_length':'You need to enter more than %(show_value)',
             },
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    estimated_budget_IGF = forms.CharField(
            max_length =200,
            initial='0',
            label = 'Estimated budget of IGF',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    estimated_budget_Donor = forms.CharField(
            max_length =200,
            initial='0',
           label = 'Estimated budget of Donor',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )


    actual_Budget_Spend = forms.CharField(
            max_length =200,
            initial='0',
            label = 'Actual budget spend',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    project_contact_person = forms.CharField(
            label = 'Project contact person',
            widget=forms.TextInput(attrs={'class': "form-control"}),
   )

    project_status = forms.ChoiceField(
        choices=PROJECT_STATUS,
        label='Project status',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )

    start_date = forms.DateField(
            initial=datetime.date.today,
            required=False,
            label = 'What\'s the start date',
            widget = forms.TextInput(attrs={'class': "form-control", 'id':'start_datepicker'}),

    )
    end_date = forms.DateField(
            initial=datetime.date.today,
            required=False,
            label = 'End date of the project',
            widget = forms.TextInput(attrs={'class': "form-control",'id':'end_datepicker'}),
    )
    completeness = forms.CharField(
     label = 'percentage complete of the project',
     required=False,
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    project_location = forms.CharField(
     label = 'The location of the project',
            widget=forms.TextInput(attrs={'class': "form-control"}),
    )
    source = forms.CharField(
            required=False,
            label = 'Source of Information',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    regions = forms.ChoiceField(
        choices=REGIONS,
        initial='Select a region',
        label='Region',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    sub_metro = forms.ChoiceField(
        choices=SUBMETROS,
        initial='Select a sub metro',
        label='Region sub metro',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    position = forms.CharField(
     label = 'Longitude coordinate',
     required = False,
            widget=forms.TextInput(attrs={'class': "form-control", 'style':'width:300px'}),
    )

    class Meta:
        model = ProjectInfo
        fields = ['project_name' ,'project_description',
                 'amount_allocated',
                 'estimated_budget_GOG', 'estimated_budget_IGF',
                 'estimated_budget_Donor' ,'actual_Budget_Spend',
                 'project_contact_person', 'project_contractor',
                 'project_status', 'project_category', 'start_date',
                 'end_date', 'completeness', 'project_location',
                 'project_image', 'source','regions','sub_metro', 'position']

def clean_something_unique_or_null(self):
        if self.cleaned_data['estimated_budget_GOG'] == "":
            return None
        else:
            return self.cleaned_data['estimated_budget_GOG']


class ProjectCommentForm(forms.ModelForm):
    commenter_name = forms.CharField(
            label = 'Name of project',
            required = False,
            widget=forms.TextInput(attrs={'class': "form-control form-1-style2 border1 borderddd",
                                          "placeholder":"your Full name"}),
   )
    comment = forms.CharField(
            label = "project description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 4}),

)


    class Meta:
        model = ProjectComment
        fields = ['commenter_name', 'comment']


class ProjectImagesForm(forms.ModelForm):

    class Meta:
        model = ProjectImage
        fields = ['additional_project_images']


# class SignupForm(forms.Form):
#     first_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
#     last_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))

#     def signup(self, request, user):
#         user.first_name = self.cleaned_data['first_name']
#         user.last_name = self.cleaned_data['last_name']
#         user.save()


class PeriodicProjectUpdateForm(forms.ModelForm):
    percentage_complete = forms.CharField(
     label = 'percentage complete of the project',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    field_visit_date = forms.DateField(
            initial=datetime.date.today,
            required=False,
            label = 'field visit date',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )

    project_status = forms.ChoiceField(
        choices=PERIODIC_PROJECT_STATUS,
        initial='Ongoing',
        label='Project status',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    comment = forms.CharField(
            label = "comments after field visit",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 5}),

    )

    class Meta:
        model = PeriodicProjectUpdate
        fields = ['percentage_complete',
                 'field_visit_date', 'project_status' ,'comment']


class EventsForm(forms.ModelForm):

    event_name = forms.CharField(
     label = 'Name of Event',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    event_description = forms.CharField(
            label = "Event description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 7, 'cols': 20}),

    )
    start_date = forms.DateField(
            initial=datetime.date.today,
            required=False,
            label = 'Date of Event',
            widget = forms.TextInput(attrs={'class': "form-control", 'id':'event_start_datepicker'}),
    )
    event_time = forms.CharField(
            required=False,
            label = 'Time of event',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    venue = forms.CharField(
            label = "Venue of the event",
            widget = forms.TextInput(attrs={'class': "form-control", 'rows': 5}),

    )
    region = forms.ChoiceField(
        choices=REGIONS,
        initial='Select a region',
        label='Region',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    sub_metro = forms.ChoiceField(
        choices=SUBMETROS,
        initial='Select a sub metro',
        label='Region\'s Sub metro',
        required=True,
        widget=forms.Select(attrs={'class': "form-control"}),
    )
    # position = forms.CharField(
    #  label = 'Longitude coordinate',
    #  required = False,
    #         widget=forms.TextInput(attrs={'class': "form-control", 'style':'width:300px'}),
    # )

    class Meta:
        model = Event
        fields = ['event_name' ,'event_description',
                 'start_date', 'event_time','venue','region', 'sub_metro',
                 'event_image']

class EventImagesForm(forms.ModelForm):
    event = forms.ModelChoiceField(
            queryset = Event.objects.all(),
            empty_label='Select One',
            label = 'Select an Event',
            required = True,
            widget=forms.Select(attrs={'class': "form-control"}),
    )

    class Meta:
        model = EventImage
        fields = ['event', 'event_image']


class BulkSMSForm(forms.ModelForm):
    phone_numbers = forms.CharField(
     label = 'Please add your phone number',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )

    class Meta:
        model = BulkSMS
        fields = ['phone_numbers']


class IVRForm(forms.ModelForm):
    phone_number = forms.CharField(
     label = 'Please add your phone number',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )

    class Meta:
        model = IVR
        fields = ['phone_number']

class SearchForm(forms.Form):
    project_category = forms.ChoiceField(
        choices=CATEGORY,
        initial='- Choose type of project -',
        widget=forms.Select(attrs={'class': "form-control custom-select select-type4 "
                                    "high location chosen-select select-type4 "
                                    "high location", "name":"project_category"}),
    )
    region = forms.ChoiceField(
        choices=REGIONS,
        initial='- Choose region -',
        widget=forms.Select(attrs={'class': "form-control custom-select select-type4 high location chosen-select select-type4 high location"}),
    )
    sub_metro = forms.ChoiceField(
        choices=SUBMETROS,
        initial='- Choose sub metro -',
        widget=forms.Select(attrs={'class': "form-control custom-select select-type4 high category chosen-select"}),
    )
class ProjectSubscriptionForm(forms.ModelForm):
    user_email = forms.CharField(
     label = 'Your email',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control subscriber_email"}),
    )
    userName = forms.CharField(
     label = 'Username',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control subscriber_username"}),
    )
    user_fullName = forms.CharField(
     label = 'Full name',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control subscriber_fullname"}),
    )
    phone_number = forms.CharField(
     label = 'Update me on project by text message and email',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control subscriber_phone_number", 'placeholder': 'please input your phone number (optional)'}),
    )

    class Meta:
        model = ProjectSubscription
        fields = ['user_email','userName','user_fullName','phone_number']


class ProjectCaseForm(forms.ModelForm):
    case_number = forms.CharField(
     label = 'Fault Number',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Fault number', 'readonly': 'readonly'}),
    )
    case_category = forms.ModelChoiceField(
     queryset = CaseCategory.objects.all(),
     label = '', #hidden label for case category
     required = False,
     widget=forms.Select(attrs={'class': "form-control",  'style': 'visibility:hidden'}),
    )
    case_name = forms.CharField(
     label = 'Fault Name',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Fault name',  'readonly': 'readonly'}),
    )
    case_description = forms.CharField(
            label = "Fault description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 5,  'readonly': 'readonly'}),

    )

    location = forms.CharField(
     label = 'Location',
     required = False,
     widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Location',  'readonly': 'readonly'}),
    )
    case_status = forms.ChoiceField(
     choices = CASE_STATUS,
     label = 'Fault status',
     required = False,
     widget=forms.Select(attrs={'class': "form-control", 'placeholder': 'Case status'}),
    )

    class Meta:
        model = ProjectCase
        fields = ['case_number', 'case_name', 'case_description',
                  'location', 'case_status']


class NewsForm(forms.ModelForm):
    title = forms.CharField(
     label = 'News title',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    description = forms.CharField(
            label = "News description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 7, 'cols': 20}),

    )
    written_by = forms.CharField(
     label = 'By',
     widget=forms.TextInput(attrs={'class': "form-control"}),
   )
    published_date = forms.DateField(
            initial=datetime.date.today,
            required=False,
            label = 'Date of News',
            widget = forms.TextInput(attrs={'class': "form-control", 'id':'news_datepicker'}),
    )


    class Meta:
        model = News
        fields = ['title' ,'description', 'written_by', 'published_date', 'news_image']
