from django.shortcuts import render, get_object_or_404
from algoliasearch_django import get_adapter
from django.core.mail import send_mail
from django.forms import modelformset_factory
from django.core.paginator import EmptyPage, InvalidPage
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from django.contrib.sites.models import Site
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import resolve
from .forms import ProjectInfoForm, ProjectCommentForm, \
                   ProjectImagesForm, PeriodicProjectUpdateForm, \
                   BulkSMSForm, IVRForm, EventsForm,SearchForm, \
                   EventImagesForm, ProjectSubscriptionForm, \
                   ProjectCaseForm, NewsForm
from django.utils import timezone
from django.shortcuts import redirect
from project.models import ProjectInfo, PeriodicProjectUpdate,ProjectSubscription, \
                           BulkSMS, IVR, Event, ProjectImage, News
from rest_api.models import CaseCategory, ProjectCase, Profile, Firebase, FaultReportStatus
from rest_api.notifications import get_firebase
from company_profile.models import CompanyProfile
from field_worker.views import display_field_worker_on_fault_detail
from field_worker.models import FieldWorker
from assign_faults.models import AssignFault

from django.template import RequestContext

import json
from django.contrib.auth.decorators import *
from random import randint
import random
from django.core.paginator import Paginator
from django.conf import settings
from gov import credInfo
from django.contrib import messages
from django.core.urlresolvers import reverse
# Django send emails
from django.template.loader import get_template
from django.template import Context

from django.http import Http404
from django.shortcuts import render_to_response
# Libraries for API
import requests
import uuid
import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import JsonResponse
from django.core import serializers
# from rest_api.serializers import *
from rest_framework.response import Response
from django.core import serializers
import csv
from django.db.models import Count
from fault_comments.models import FaultComment
# configuration for error 404 and 500 page
def handler500(request):
   return render_to_response('project/500.html', context_instance=RequestContext(request))
# UEOMOIZI wifi pass

# def handler404(request):
#    return render_to_response('landingPage/searchResults.html', context_instance=RequestContext(request))


def handler404(request):
    response = render_to_response(
        '404.html',
        context_instance=RequestContext(request)
    )
    response.status_code = 404
    return response


# STATIC PAGE VIEWS
# search bar on home page code, this search is not well implemented
def index_page(request):

    message_success_color = '#3bac84;'
    if request.method == "POST":
        form = SearchForm(request.POST)
        getProjCatVal =  request.POST['project_category']
        getRegionVal =  request.POST['project_category']
        getSub_metroVal =  request.POST['project_category']
        getProjectCat = ProjectInfo.objects.filter(project_category = getProjCatVal)
        getRegion = ProjectInfo.objects.filter(regions = getRegionVal)
        getSub_metro = ProjectInfo.objects.filter(sub_metro = getSub_metroVal)
        mySearch = getProjectCat | getRegion | getSub_metro
        if mySearch:
            paginator = Paginator(mySearch, 6)
            try:
                page = int(request.GET.get('page', '1'))
            except:
                page = 1
            try:
                mySearch = paginator.page(page)
            except(EmptyPage, InvalidPage):
                mySearch = paginator.page(paginator.num_pages)
            context = {
                'search_results': mySearch,
                'color': message_success_color
            }
            # messages.success(request, 'Your search was found')
            return render(request, "landingPage/searchResults.html", context)
    else:
        message_success_color = '#EA7878;'
        # messages.error(request, 'Oops! No search results found')
        form = SearchForm()
    return render(request, 'landingPage/index.html',{'form':form})

def search_results(request):
    return render(request, 'landingPage/searchResults.html')

def about_transgov(request):
    return render(request, 'landingPage/about.html')

def partners(request):
    return render(request, 'landingPage/partnersInfo.html')

def contact(request):
    return render(request, 'landingPage/contact.html')

def notfound(request):
    return render(request, 'landingPage/404.html')

def privacy_policy(request):
    return render(request, 'landingPage/privacy_policy.html')



# DASHBOARD ACTIVITY VIEWS
@login_required()
def transgov_dashboard(request):
    if request.user.is_superuser or request.user.is_staff:
        # count project status on dashboard
        count_completed_projects = ProjectInfo.objects.filter(project_status="Completed").count()
        count_ongoing_projects = ProjectInfo.objects.filter(project_status="Ongoing").count()
        count_halted_projects = ProjectInfo.objects.filter(project_status="On Halt").count()

        # Count type of projects on dashboard
        count_schools = ProjectInfo.objects.filter(project_category="school").count()
        count_road_and_highways = ProjectInfo.objects.filter(project_category="roads and highways").count()
        count_hospitals = ProjectInfo.objects.filter(project_category="hospitals").count()
        count_housing = ProjectInfo.objects.filter(project_category="housing").count()
        count_toilet_facilities = ProjectInfo.objects.filter(project_category="toilet facilities").count()
        count_borehole = ProjectInfo.objects.filter(project_category="borehole").count()

        # Recent activities #Catch exception here
        try:
            comp_profile = CompanyProfile.objects.get(user=request.user)
            if comp_profile:
                company_type = comp_profile.company_interest
                recent_report = ProjectCase.objects.all().order_by('-id'
                    ).filter(case_category__in=company_type)[:3]
        except CompanyProfile.DoesNotExist:
            return render(request, 'project/not_attached_to_company.html')

        # Count Number of reports from each category
        total_faults = ProjectCase.objects.all().count()
        count_water = ProjectCase.objects.filter(case_category__category_name="Water").count()
        count_electricity = ProjectCase.objects.filter(case_category__category_name="Electricity").count()
        count_drainage = ProjectCase.objects.filter(case_category__category_name="Drainage").count()
        count_construction = ProjectCase.objects.filter(case_category__category_name="Poor construction").count()
       
        
      # Count Faults reported on Dashboard based on status
        reports = ProjectCase.objects.filter(case_category__in=company_type)
        if request.user.is_superuser:
            reports = ProjectCase.objects.all()

        
        count_reports = {
            'pending_faults': reports.filter(case_status="pending").count(),
            'fixing_faults': reports.filter(case_status="Being fixed").count(),
            'fixed_faults': reports.filter(case_status="Fault fixed").count(),
        }
        
        context  = {
            'count_completed_projects': count_completed_projects,
            'count_ongoing_projects': count_ongoing_projects,
            'count_halted_projects': count_halted_projects,
            # Count type of projects
            'count_schools': count_schools,
            'count_road_and_highways': count_road_and_highways,
            'count_hospitals': count_hospitals,
            'count_housing': count_housing,
            'count_toilet_facilities': count_toilet_facilities,
            'count_borehole': count_borehole,

            'count_reports': count_reports, # count_reports context
            'recent_report': recent_report, # show recent faults reports.

            # count reports category
            'total_faults': total_faults,
            'water': count_water,
            'electricity':count_electricity,
            'drainage':count_drainage,
            'construction':count_construction
        }
        return render(request, 'project/dashboard_content.html', context)
    else:
        return render(request, 'project/not_attached_to_company.html')


# This section is to add project information from TransGov Dashboard
@login_required()
def add_project_info(request):
    message_success_color = '#3bac84'  # default color code when project info is added successfully
    if request.method == "POST":
        project_info_form = ProjectInfoForm(request.POST, request.FILES)
        if project_info_form.is_valid():
            project_info_form = project_info_form.save(commit=False)
            project_info_form.author = request.user
            project_info_form.published_date = timezone.now()
            project_info_form.save()
            messages.success(request, 'You have successfully added a project information',
                             extra_tags='success_message')
            return redirect('addProjectInfo')
        else:
            message_success_color = '#EA7878;'  # color code when project info is not added.
            messages.error(request, 'Project information could not be added')
    else:
            project_info_form = ProjectInfoForm()
    return render(request, 'project/add_project_info.html', {'form': project_info_form,
                                                             'color':message_success_color})


# This section is for edit Project Information on TransGov Dashboard
@login_required()
def edit_project_info(request, pk):
    message_success_color = '#3bac84' # default color code when project info is added successfully
    project_info = get_object_or_404(ProjectInfo, pk=pk)
    if request.method == "POST":
        project_info_form = ProjectInfoForm(request.POST, request.FILES, instance=project_info)
        if project_info_form.is_valid():
            project_info_form = project_info_form.save(commit=False)
            project_info_form.author = request.user
            project_info_form.published_date = timezone.now()
            project_info_form.save()
            messages.success(request, 'You have successfully added a project information',
                             extra_tags='success_message')
            return redirect('viewProjectsonDashboard')
        else:
            message_success_color = '#EA7878;'
            messages.error(request, 'Project information could not be added')
    else:
            project_info_form = ProjectInfoForm(instance=project_info)
    return render(request, 'project/edit_project_info.html', {'form': project_info_form,
                                                              'color':message_success_color})


# This section is to view projects from the dashboard
@login_required()
def project_list_on_dashboard(request):
    project_list = ProjectInfo.objects.all().order_by('-id')
    context ={
       'project_list': project_list
    }
    return render(request, 'project/adminView_project_list.html', context)


# Project details page on TransGov dashboard
@login_required()
def project_details_on_dashboard(request, pk):
    project_details_on_dashboard = get_object_or_404(ProjectInfo, pk=pk)
    context = {
        'pro_dash_details': project_details_on_dashboard
    }
    return render(request, 'project/admin_project_detailView.html', context)


# Select project to add images
@login_required()
def select_proj_add_image(request):
    project_list = ProjectInfo.objects.all().order_by('-id')
    context ={
       'project_list': project_list
    }
    return render(request, 'project_images/proj_list_add_image.html', context)


@login_required()
def add_images_to_project(request, pk):
    project_info = get_object_or_404(ProjectInfo, pk=pk)
    ProjectImagesFormSet = modelformset_factory(ProjectImage, form=ProjectImagesForm, extra=2)
    if request.method == "POST":
        formset = ProjectImagesFormSet(request.POST, request.FILES)
        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.author = request.user
                instance.project = project_info
                instance.published_date = timezone.now()
                instance.save()
        return redirect('addProjectInfo')
    else:
        proj_image_form = ProjectImagesFormSet()

    context = {'project_info_details':project_info,
               'formset': proj_image_form
               }
    return render(request, 'project_images/add_project_images.html', context)


# This section is to add periodic project update information
@login_required()
def add_periodic_project_update(request, pk):
    project_details = get_object_or_404(ProjectInfo, pk=pk)
    if request.method == "POST":
        periodic_project_update_form = PeriodicProjectUpdateForm(request.POST, request.FILES)
        if periodic_project_update_form.is_valid():
            periodic_project_update_form = periodic_project_update_form.save(commit=False)
            periodic_project_update_form.author = request.user
            periodic_project_update_form.project_id = project_details.id
            periodic_project_update_form.project_name = project_details.project_name
            periodic_project_update_form.published_date = timezone.now()
            periodic_project_update_form.save()
            return redirect('periodicProjectUpdateList')
    else:
            periodic_project_update_form = PeriodicProjectUpdateForm()
    return render(request,
                  'project/add_periodic_project_update.html',
                  {'project_details':project_details,
                  'form': periodic_project_update_form})


# This section is to edit periodic project update information
@login_required()
def edit_periodic_project_update(request, pk):
    periodic_project_update = get_object_or_404(PeriodicProjectUpdate, pk=pk)
    if request.method == "POST":
        periodic_project_update_form = PeriodicProjectUpdateForm(request.POST, request.FILES)
        if periodic_project_update_form.is_valid():
            periodic_project_update_form = periodic_project_update_form.save(commit=False)
            periodic_project_update_form.author = request.user
            periodic_project_update_form.published_date = timezone.now()
            periodic_project_update_form.save()
            return redirect('periodicProjectUpdateList')
    else:
            periodic_project_update_form = PeriodicProjectUpdateForm(instance=periodic_project_update)
    return render(request,
                  'project/edit_periodic_project_update.html',
                  {'form': periodic_project_update_form})


# This section is to view list of projects for periodic update
@login_required()
def periodic_update_project_list(request):
    project_list = ProjectInfo.objects.all().order_by('id')
    context = {
       'project_list': project_list
    }
    return render(request, 'project/periodic_projectUpdate_list.html', context)


# This section is to add BulkSMS numbers
@login_required()
def add_bulkSMS_number(request):
    message_success_color = '#3bac84'
    if request.method == "POST":
        bulk_sms_form = BulkSMSForm(request.POST, request.FILES)
        if bulk_sms_form.is_valid():
            bulk_sms_form = bulk_sms_form.save(commit=False)
            bulk_sms_form.author = request.user
            bulk_sms_form.published_date = timezone.now()
            bulk_sms_form.save()
            messages.success(request, 'you have successfully added a contact',
                             extra_tags='success_message')
            return redirect('addBulkSMSNumber')
        else:
            message_success_color = '#EA7878;'
            messages.error(request, 'Contact could not be added')
    else:
            bulk_sms_form = BulkSMSForm()

    return render(request, 'bulk_sms/add_bulkSMS_number.html',
                  {'form': bulk_sms_form, 'color':message_success_color} )


# This section is to view BulkSMS contacts on the dashboard
@login_required()
def bulkSMS_list_on_dashboard(request):
    bulkSMS_list = BulkSMS.objects.all().order_by('-id')
    context ={
       'bulk_sms_list': bulkSMS_list
    }
    return render(request, 'bulk_sms/list_of_bulkSMS_numbers.html',context)


# This section is to add IVR contacts
@login_required()
def add_IVR_number(request):
    message_success_color = '#3bac84'
    if request.method == "POST":
        IVR_form = IVRForm(request.POST, request.FILES)
        if IVR_form.is_valid():
            IVR_form = IVR_form.save(commit=False)
            IVR_form.author = request.user
            IVR_form.published_date = timezone.now()
            IVR_form.save()
            messages.success(request, 'you have successfully added contact for IVR',
                             extra_tags='success_message')
            return redirect('addIVRContact')
        else:
            message_success_color = '#EA7878;'
            messages.error(request, 'Contact could not be added for IVR')
    else:
            IVR_form = IVRForm()
    return render(request, 'ivr_contacts/add_ivr_contact.html', {'form': IVR_form,
                                                                 'color':message_success_color})


# This section is to view BulkSMS contacts on the dashboard
@login_required()
def ivr_list_on_dashboard(request):
    ivr_contact_list = IVR.objects.all().order_by('-id')
    context ={
       'ivr_list': ivr_contact_list
    }
    return render(request, 'ivr_contacts/ivr_contact_list.html',context)


@login_required()
def add_event(request):
    message_success_color = '#3bac84'
    if request.method == "POST":
        event_form = EventsForm(request.POST, request.FILES)
        if event_form.is_valid():
            event_form = event_form.save(commit=False)
            event_form.author = request.user
            event_form.published_date = timezone.now()
            event_form.save()
            messages.success(request, 'you have successfully added an event',
                             extra_tags='success_message')
            return redirect('addEvent')
    else:
        event_form = EventsForm()
    return render(request, 'events/add_event.html', {'form': event_form,
                                                     'color':message_success_color})


# This is side attaches more images to projects
@login_required()
def add_event_images(request):
    message_success_color = '#3bac84'
    if request.method == "POST":
        event_image_form = EventImagesForm(request.POST, request.FILES)
        if event_image_form.is_valid() and Event.objects.get(id = request.POST['event']):
            event_image_form = event_image_form.save(commit=False)
            event_image_form.author = request.user
            event_image_form.event = Event.objects.get(id = request.POST['event'])
            event_image_form.published_date = timezone.now()
            event_image_form.save()
            messages.success(request, 'you have successfully added an event Image',
                             extra_tags='success_message')
            return redirect('addEventImages')
        else:
            message_success_color = '#EA7878;'
            messages.error(request, 'Oops! Event image could not be added')
    else:
            event_image_form = EventImagesForm()
    return render(request, 'events/add_event_images.html', {'form': event_image_form,
                                                            'color': message_success_color})


@login_required()
def list_of_events_dashboard(request):
    event_list = Event.objects.all().order_by('-id')
    context ={
       'list_of_events': event_list
    }
    return render(request, 'events/listOfEvents.html',context)


@login_required()
def event_details_on_dashboard(request, pk):
    event_details_on_dashboard = get_object_or_404(Event, pk=pk)

    return render(request, 'events/event_details.html',
                  {'dashboard_event_detail':event_details_on_dashboard})


@login_required()
def edit_event(request, pk):
    message_success_color = '#3bac84'
    edit_event = get_object_or_404(Event, pk=pk)
    if request.method == "POST":
        event_update_form = EventsForm(request.POST, request.FILES, instance=edit_event)
        if event_update_form.is_valid():
            event_update_form = event_update_form.save(commit=False)
            event_update_form.author = request.user
            event_update_form.published_date = timezone.now()
            event_update_form.save()
            messages.success(request, 'you have successfully added an event Image',
                             extra_tags='success_message')
            return redirect('eventsList')
        else:
            message_success_color = '#EA7878;'
            messages.error(request, 'Oops! Event could not be edited')
    else:
            event_update_form = EventsForm(instance=edit_event)
    return render(request,
                  'events/edit_event.html',
                  {'form': event_update_form, 'color': message_success_color})


@login_required()
def delete_event(request, pk):
    get_event = get_object_or_404(Event, pk=pk)
    get_event.delete()
    return redirect('eventsList')


def admin_insight(request):
    return render (request, 'diy_cases/admin_insights.html')

# show all faults if TransGov admin and filter if TransGov staff
@login_required()
def all_diy_cases(request):
    if request.user.is_superuser:
        company_profile = CompanyProfile.objects.get(user=request.user)
        # Check for company profile type and display export button accordingly

        all_cases = ProjectCase.objects.all().order_by('-id')
        paginator = Paginator(all_cases, 6)
        try:
            page = int(request.GET.get('page', '1'))
        except:
            page = 1
        try:
            all_cases = paginator.page(page)
        except(EmptyPage, InvalidPage):
            all_cases = paginator.page(paginator.num_pages)
        context = {
            "all_diy_cases": all_cases,
            }
            

        return render(request, 'diy_cases/all_diy_cases.html', context)
    elif request.user.is_staff:
        company_profile = CompanyProfile.objects.get(user=request.user).company_interest
        all_cases = ProjectCase.objects.all().order_by('-id').filter(
            case_category__in=company_profile)

        paginator = Paginator(all_cases, 6)
        try:
            page = int(request.GET.get('page', '1'))
        except:
            page = 1
        try:
            all_cases = paginator.page(page)
        except(EmptyPage, InvalidPage):
            all_cases = paginator.page(paginator.num_pages)

        context = {
            "all_diy_cases": all_cases
        }
        return render(request, 'diy_cases/all_diy_cases.html', context)
    else:
        return render(request, 'project/not_authorized.html')


@login_required()
def pending_cases(request):
    # pending_cases = ProjectCase.objects.filter(case_status="pending").order_by('-id')
    company_profile = CompanyProfile.objects.get(user=request.user)
    pending_cases = ProjectCase.objects.filter(case_status="pending",
        case_category__in=company_profile.company_interest)
    paginator = Paginator(pending_cases, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        pending_cases = paginator.page(page)
    except(EmptyPage, InvalidPage):
        pending_cases = paginator.page(paginator.num_pages)
    context = {
        "pending_cases": pending_cases,
    }
    return render(request, 'diy_cases/pending_cases.html', context)


@login_required()
def fixing_cases(request):
    # fixing_cases = ProjectCase.objects.filter(case_status="Being fixed").order_by('-id')
    company_profile = CompanyProfile.objects.get(user=request.user)
    fixing_cases = ProjectCase.objects.filter(case_status="Being fixed",
        case_category__in=company_profile.company_interest)
    paginator = Paginator(fixing_cases, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        fixing_cases = paginator.page(page)
    except(EmptyPage, InvalidPage):
        fixing_cases = paginator.page(paginator.num_pages)
    context = {
        "ongoing_fixes_cases": fixing_cases,
    }
    return render(request, 'diy_cases/fixing_cases.html', context)


@login_required()
def fixed_cases(request):
    company_profile = CompanyProfile.objects.get(user=request.user)
    fixed_cases = ProjectCase.objects.filter(case_status="Fault fixed",
        case_category__in=company_profile.company_interest)
    paginator = Paginator(fixed_cases, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        fixed_cases = paginator.page(page)
    except(EmptyPage, InvalidPage):
        fixed_cases = paginator.page(paginator.num_pages)
    context = {
        "fixed_cases": fixed_cases,
    }
    return render(request, 'diy_cases/fixed_cases.html', context)


@login_required()
def case_details(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        case_details = get_object_or_404(ProjectCase, pk=pk)
        get_user = case_details.author

        # Try get the profile object.
        get_prof = ""
        try:
            get_prof = Profile.objects.select_related().get(user = get_user)
            
        except Profile.DoesNotExist:
            get_prof = None
        

        #assign faults to field worker code
        assign_btn_text = 'Assign'  # button text change in html
        get_case = get_object_or_404(ProjectCase, pk=pk) # get current fault to assign to field worker
        field_workers = display_field_worker_on_fault_detail(request) # get field workers for this company
        company_profile = company_profile = CompanyProfile.objects.get(
            user=request.user
            ) # get company for this fault
        assign_faults = AssignFault.objects.filter(
            community_fault=get_case
            ) # get the assigned fault based on this company.

        field_workers_id = []
        for faults in assign_faults:
            field_workers_id.append(faults.field_worker.id)

        all_field_worker = []
        for worker in field_workers:
            worker.assigned = False # New attribute assigned
            if worker.id in field_workers_id:
                worker.assigned = True
            all_field_worker.append(worker)
        
        if request.method == "POST":
            field_worker_id = request.POST.get('field_worker_id')
            field_worker = FieldWorker.objects.get(id=field_worker_id)
            assign_fault = AssignFault.objects.create(
                community_fault = get_case,
                field_worker = field_worker,
                date_assigned = timezone.now(),
                author = request.user
                )

        comments = FaultComment.objects.filter(fault=case_details)

        context = {
            'case_detail': case_details,
            'case_author_cred': get_prof,
            'field_workers': all_field_worker,
            'comments': comments,
            'count_comment':comments.count()
        }
        return render(request, 'diy_cases/case_details.html', context)
    else:
        return render(request, 'project/not_authorized.html')






# @login_required()
def delete_fault_assignment(request):
    if request.method == "POST":
        field_worker_id = request.POST['field_worker_id']
        fault_id = request.POST['fault_id']
        field_worker = FieldWorker.objects.get(id=field_worker_id)
        assign_fault = AssignFault.objects.get(
            field_worker = field_worker, community_fault=fault_id
            )
        assign_fault.delete()
        context = {
        "assign_fault_id": assign_fault.id
        }
        return render(request, 'project/not_authorized.html')

@login_required()
def delete_case(request, pk):
    if request.user.is_superuser:
        get_case = get_object_or_404(ProjectCase, pk=pk)
        get_case.delete()
        return redirect('viewAllCasesOnDashboard')
    else:
        return render(request, 'project/not_authorized.html')


@login_required()
def edit_project_case(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        project_case = get_object_or_404(ProjectCase, pk=pk)
        if request.method == "POST":
            case_update_form = ProjectCaseForm(request.POST, request.FILES, instance=project_case)
            if case_update_form.is_valid():
                case_update_form = case_update_form.save(commit=False)
                case_update_form.save()
                return redirect('caseDetailsOnDashboard', pk=pk)
        else:
            case_update_form = ProjectCaseForm(instance=project_case)
            context = {
                'form': case_update_form
            }
            return render(request, 'diy_cases/edit_case.html', context)
    else:
        return render(request, 'project/not_authorized.html')


# All reports status or logs for transgov Dashboard
@login_required()
def allfaultreportstatus(request):
    if request.user.is_superuser or request.user.is_staff:
        # Check for company profile type and display case status
        try:
            comp_profile = CompanyProfile.objects.get(user=request.user)
            company_type = comp_profile.company_type
            all_report_status = ProjectCase.objects.all().order_by('-id'
                )


            paginator = Paginator(all_report_status, 9)
            try:
                page = int(request.GET.get('page', '1'))
            except:
                page = 1
            try:
                all_report_status = paginator.page(page)
            except(EmptyPage, InvalidPage):
                all_report_status = paginator.page(paginator.num_pages)
            context = {
                "report_status": all_report_status
            }


            return render(request, 'fault_report_status/all_report_status.html', context)
        except CompanyProfile.DoesNotExist:
            return redirect('create_company_profile')
    else:
        return render(request, 'project/not_authorized.html')


# specific report status or logs for utility agencies
@login_required()
def specificfaultreportstatus(request):
    if request.user and request.user.is_staff:
        # Check for company profile type and display case status
        try:
            comp_profile = CompanyProfile.objects.get(user=request.user)
            specific_report_status = ProjectCase.objects.all().order_by('-id'
                ).filter(case_category__in=comp_profile.company_interest)
            paginator = Paginator(specific_report_status, 9)
            try:
                page = int(request.GET.get('page', '1'))
            except:
                page = 1
            try:
                specific_report_status = paginator.page(page)
            except(EmptyPage, InvalidPage):
                specific_report_status = paginator.page(paginator.num_pages)
            context = {
                "report_status": specific_report_status
            }
            return render(request, 'fault_report_status/specific_report_status.html', context)
        except CompanyProfile.DoesNotExist:
            return redirect('create_company_profile')
    else:
        return render(request, 'project/not_authorized.html')


# Search fault
def fault_search(request):
    context = {
            'appID': settings.ALGOLIA['APPLICATION_ID'],
            'searchKey': settings.ALGOLIA['SEARCH_API_KEY'],
            'indexName': get_adapter(ProjectCase).index_name

            }
    return render(request, 'diy_cases/search_fault.html')

@login_required()
def add_news(request):
    if request.user.is_superuser:
        add_news_form = NewsForm(request.POST or None, request.FILES)
        success = False
        if request.method == 'POST':
            if add_news_form.is_valid():
                add_news_form = add_news_form.save(commit=False)
                add_news_form.author = request.user
                add_news_form.save()
                add_news_form = NewsForm()
                success = True
                messages.add_message(request, messages.INFO, 'News saved successfully')
        else:
            add_news_form = NewsForm()
        return render(request, 'news/add_news.html', {'form': add_news_form})
    else:
        return render(request, 'project/not_authorized.html')


@login_required
def edit_news(request, pk):
    if request.user.is_superuser:
        news = get_object_or_404(News, pk=pk)
        if request.method == 'POST':
            news_form = NewsForm(request.POST, request.FILES, instance=news)
            if news_form.is_valid():
                news_form = news_form.save(commit=False)
                news_form.author = request.user
                news_form.save()
                messages.add_message(request, messages.INFO, 'News edited successfully')
                return redirect('news_list')
        else:
            news_form = NewsForm(instance=news)
        return render(request, 'news/edit_news.html', {'form': news_form})
    else:
        return render(request, 'project/not_authorized.html')


# list of news recorded on dashboard with pagination
@login_required
def news_list(request):
    if request.user.is_superuser:
        list_of_news = News.objects.all().order_by('-id')
        paginator = Paginator(list_of_news, 6)
        try:
            page = int(request.GET.get('page', '1'))
        except:
            page = 1
        try:
            list_of_news = paginator.page(page)
        except(EmptyPage, InvalidPage):
            list_of_news = paginator.page(paginator.num_pages)
        context = {
        'news_list': list_of_news
        }
        return render(request, 'news/news_list.html', context)
    else:
        return render(request, 'project/not_authorized.html')


@login_required()
def dashboard_news_details(request, pk):
    if request.user.is_superuser:
        dash_news_details = get_object_or_404(News, pk=pk)

        return render(request, 'news/dash_news_details.html',
                      {'news_details': dash_news_details})
    else:
        return render(request, 'project/not_authorized.html')


# Delete news from dashboard
@login_required()
def delete_news(request, pk):
    if request.user.is_superuser:
        get_news = get_object_or_404(News, pk=pk)
        get_news.delete()
        return redirect('news_list')
    else:
        return render(request, 'project/not_authorized.html')


# Export reported faults data to csv
# def export_reported_faults_csv(request):
#     response = HttpResponse(content_type='text/csv')
#     response['Content-Disposition'] = 'attachment; filename="users.csv"'

#     writer = csv.writer(response)
#     writer.writerow(['Case number', 'Case category', 'Case name', 'description'])

#     reported_cases = ProjectCase.objects.all().values_list('case_number', 'case_category', 'case_name', 'case_description')
#     for cases in reported_cases:
#         writer.writerow(cases)
#     return response



def export_fault_as_csv(request):
    comp_profile = CompanyProfile.objects.get(user=request.user).company_interest
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="TransGov_faults_report.csv"'
    writer = csv.writer(response)
    writer.writerow(['Case number', 'Fault category', 'Fault name', 'description', 
                    'region', 'suburb', 'Fault status', 'Latitude', 'Longitude'])
    reported_cases = ProjectCase.objects.filter(case_category__in=comp_profile).values_list(
            'case_number', 'case_category__category_name', 'case_name', 'case_description', 
            'region__region', 'suburb__suburb', 'case_status', 'latitude', 'longitude')
    for cases in reported_cases:
        writer.writerow(cases)
    return response


# def export_ecg_report_csv(request):
#     response = HttpResponse(content_type='text/csv')
#     response['Content-Disposition'] = 'attachment; filename="TransGov_ecg_cases_report.csv"'
#     writer = csv.writer(response)
#     writer.writerow(['Case number', 'Case name', 'description', 'region', 'suburb',
#                         'case status', 'land mark', 'map land mark', 'longitude', 'latitude'])
#     reported_cases = ProjectCase.objects.filter(case_category__category_name__exact="ECG").values_list(
#             'case_number', 'case_name', 'case_description', 'region', 'suburb','case_status',
#             'land_mark', 'map_land_mark_name', 'longitude', 'latitude')
#     for cases in reported_cases:
#         writer.writerow(cases)
#     return response





# MAIN SITE VIEWS
# This section is to view all projects by the public with pagination
def public_view_projects(request):
    # check if user has already liked project, then mark it as liked.
    # disabling like page for 
    # after_like = "list-page-like"  # Default like button class
    # user = request.user
    # if request.user.is_authenticated():
    #     after_like = {}  # create a dictionary to look through for some data
    #     for project_likes in ProjectInfo.objects.filter(likes=user):
    #         if project_likes:
    #             after_like[project_likes.id] = "after_like"  #get project id from the dictionary and pass variable to it.

    # List projects on projects view page and order by id.
    public_project_list = ProjectInfo.objects.all().order_by('-id')
    paginator = Paginator(public_project_list, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        public_project_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        public_project_list = paginator.page(paginator.num_pages)
    context ={
       'projectList': public_project_list,
       # 'after_like': after_like #like feature disabled
    }
    return render(request, 'project/all_projects.html', context)


def project_likes(request):
    user = request.user
    project_id = request.POST.get('project_id')
    project_information = ProjectInfo.objects.get(id=project_id) ## You get the object with the id
    if user not in project_information.likes.all():
        project_information.likes.add(user)
        project_information.count_likes += 1
        project_information.save()
    return render(request, "project/Project_detailView.html")


def listing_page_project_likes(request):
    user = request.user
    project_id = request.POST.get('project_id')
    project_information = ProjectInfo.objects.get(id=project_id) ## You get the object with the id
    # if request.user.is_authenticated():
    try:
        if user not in project_information.likes.all():
            project_information.likes.add(user)
            project_information.count_likes += 1
            project_information.save()
            context = {
             "likes_count": project_information.count_likes
            }
            return render(request, "project/all_projects.html", context)
    except ValueError:
        pass


# This section is to view project details for public
def project_details(request, pk):
    like_button_class = ""  # Default like button class
    user = request.user
    project_info = get_object_or_404(ProjectInfo, pk=pk)
    if user in project_info.likes.all():  # check if user has liked project
        like_button_class = "like-thumbs-up"
    message_success_color = '#3bac84' # default color code when project info is added successfully
    subscription_url =  '/accounts/signup'
    subscription_text = 'Login to subscribe'
    get_subscriber_status = ''
    subscribe_btn_id=''
    get_status_info =''
    project_details = get_object_or_404(ProjectInfo, pk=pk)
    # get subscriber status and email
    # check state of subscription
    if request.user.is_authenticated():
        for info in ProjectSubscription.objects.filter(user_email=request.user.email).filter(projectId = project_details.id):
            get_status_info = info.subscribe_status
        get_subscriber_status = get_status_info if get_status_info == "YES" else "NO"

    # Show similar projects by sub_metro on project details page for public
    for e in ProjectInfo.objects.filter(pk=pk):
        similar_project_submetro = e.sub_metro
        similar_projects = ProjectInfo.objects.filter(sub_metro = similar_project_submetro).order_by('?')
        similar_sub_metro_projects = similar_projects

        # Project progress update
        get_project_info = ProjectInfo.objects.get(pk=pk)
        project_updates = PeriodicProjectUpdate.objects.filter(project_id=get_project_info.id).all().order_by('id')

    if request.method == "POST":
        subscribe_form = ProjectSubscriptionForm(request.POST)
        if subscribe_form.is_valid():
            subscribeInfo = ProjectSubscription()
            # lists all the variables from ajax in an array and loop to get items.
            variablesFromAjax = ["phone_number", "user_email", "project_name","userName",
                       "user_fullName","current_url","projectId","subscribe_status"]
            for eachVariable in variablesFromAjax: # using for loop to get items
                    setattr(subscribeInfo, eachVariable, request.POST[eachVariable])
            subscribeInfo.author = request.user
            subscribeInfo.save()
            return render(request, "project/Project_detailView.html")
    else:
        subscribe_form = ProjectSubscriptionForm()
        # Checking to display right text to subscribed user
        if get_subscriber_status == 'YES':
            subscription_text = 'Unsubscribe'
            subscription_url = "#"
            subscribe_btn_id = "unsubscribe-btn"
            # messages.success(request, 'You have successfully subscribed to this project')
        elif get_subscriber_status == 'NO':
            subscription_text = 'Subscribe'
            subscribe_btn_id = "subscribe-btn"
            subscription_url = "#subscribe_to_project"
            message_success_color = '#EA7878'
            # messages.error(request, 'Oops! Something went wrong, you could\'t subscribe to project')
           
        context = {
            'project_details': project_details,
            'project_id': pk,
            'similar_sub_metro_projects': similar_sub_metro_projects,
            'project_updates': project_updates,
            'get_subscriber_status':get_subscriber_status,
            'form': subscribe_form,
            'subscribe_btn_id': subscribe_btn_id,
            'subscription_text': subscription_text, 'subscription_url': subscription_url,
            'color': message_success_color,
            'like_btn_class': like_button_class,
        }
        return render(request, 'project/Project_detailView.html', context)


   # Delete subscriber
def delete_subscriber(request, pk):
    if request.method == "GET":
        get_project_subscribe_details = ProjectSubscription.objects.filter(user_email=request.user.email).filter(projectId=pk)
        if get_project_subscribe_details:
            get_project_subscribe_details.delete()
            #messages.success(request, 'You have successfully unsubscribed to this project')
            return {"status":"0", "message":"You have unsubscribed to this project"}
        return {"status":"1", "message":"Failed to unsubscribe"}


# send successful text message to subscriber with the project name
@receiver(post_save, sender=ProjectSubscription)
def send_subscribe_sms(sender, instance, **kwargs):
    r = requests.get('https://api.smsgh.com/v3/messages/send?ClientId=' + credInfo.CLIENTID +
                             '&ClientSecret=' + credInfo.CLIENTSECRET + '&From=' + settings.FROM +
                             '&To=' + instance.phone_number + '&RegisteredDelivery=' + settings.REGISTEREDDELIVERY +
                             '&Content=' + "You have just subscribed to " + instance.project_name +
                             ". You will be notified anytime there is an update. you can check www.transgovgh.org "
                             "for more information. Thank You")
    status = r.status_code
    if status == 201:
        print("Text message has been successfully delivered")
    elif status == 400:
        print("Message not delivered")


# sends successful email to subscriber
@receiver(post_save, sender=ProjectSubscription)
def send_success_email_to_subscriber(instance, **kwargs):
    htmly = get_template('sendEmails/project_successfully_subscribe_email.html')
    plaintext = get_template('sendEmails/project_successfully_subscribe_email.txt')
    d = Context({ 'full_name': instance.user_fullName,
                  'project_name': instance.project_name})
    user_full_name = instance.user_fullName
    email_subject = 'Welcome' + " " + str(user_full_name) + " " + 'to TransGov'
    # email_body = htmly
    to=instance.user_email
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                                 settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


#   Send Email to transGov to when someone subscribes to a project
@receiver(post_save, sender=ProjectSubscription)
def send_subscription_info_to_transGov(sender, instance, **kwargs):
    htmly = get_template('sendEmails/send_subscription_info_to_transgov.html')
    plaintext = get_template('sendEmails/send_subscription_info_to_transgov.txt')
    d = Context({ 'full_name': instance.user_fullName,
                  'project_name': instance.project_name,
                  'username':instance.userName,
                  'phone_number': instance.phone_number,
                  'current_url':instance.current_url})
    email_subject = 'TransGov Project Subscription'
    # replace email with transgov subscription email for production
    to=credInfo.TRANSGOV_EMAIL_SUBSCRIPTION
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                                 settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def public_view_event(request):
    public_view_event = Event.objects.all().order_by('-id')
    paginator = Paginator(public_view_event, 1)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        public_view_event = paginator.page(page)
    except(EmptyPage, InvalidPage):
        public_view_event = paginator.page(paginator.num_pages)
    context ={
       'public_events': public_view_event
    }
    return render(request, 'events/public_event_list.html', context)


def event_details_for_public(request, pk):
    public_view_event_details = get_object_or_404(Event, pk=pk)

    return render(request, 'events/public_event_detail.html',
                  {'public_event_detail': public_view_event_details})


# Public view of the news
def public_news_list(request):
    list_of_news = News.objects.all().order_by('-id')
    paginator = Paginator(list_of_news, 1)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        list_of_news = paginator.page(page)
    except(EmptyPage, InvalidPage):
        list_of_news = paginator.page(paginator.num_pages)
    context = {
    'news_list': list_of_news
    }
    return render(request, 'news/public_news_list.html', context)


def news_details_for_public(request, pk):
    public_news_details = get_object_or_404(News, pk=pk)
    return render(request, 'news/public_news_details.html',
                  {'public_news_detail': public_news_details})

def help_page(request):
    return render(request, 'project/help.html')