# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-27 22:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0036_projectsubscription_current_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectsubscription',
            name='current_url',
            field=models.CharField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectsubscription',
            name='projectId',
            field=models.CharField(default=1, max_length=150),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectsubscription',
            name='project_name',
            field=models.CharField(default=1, max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectsubscription',
            name='userName',
            field=models.CharField(default=1, max_length=150),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectsubscription',
            name='user_email',
            field=models.CharField(default=1, max_length=150),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectsubscription',
            name='user_fullName',
            field=models.CharField(default=1, max_length=150),
            preserve_default=False,
        ),
    ]
