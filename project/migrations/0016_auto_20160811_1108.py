# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-11 11:08
from __future__ import unicode_literals

from decimal import Decimal
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0015_auto_20160810_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectinfo',
            name='actual_Budget_Spend',
            field=models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.000'), max_digits=20, null=True),
        ),
        migrations.AlterField(
            model_name='projectinfo',
            name='amount_allocated',
            field=models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.000'), max_digits=20, null=True),
        ),
        migrations.AlterField(
            model_name='projectinfo',
            name='estimated_budget_Donor',
            field=models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.000'), max_digits=20, null=True),
        ),
        migrations.AlterField(
            model_name='projectinfo',
            name='estimated_budget_GOG',
            field=models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.000'), max_digits=20, null=True),
        ),
        migrations.AlterField(
            model_name='projectinfo',
            name='estimated_budget_IGF',
            field=models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.000'), max_digits=20, null=True),
        ),
    ]
