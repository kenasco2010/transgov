# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-18 23:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0027_remove_bulksms_author'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectinfo',
            name='project_links',
        ),
        migrations.AddField(
            model_name='projectinfo',
            name='source',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
    ]
