# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-22 21:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0018_auto_20160816_1334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectsubscription',
            name='project',
        ),
        migrations.RemoveField(
            model_name='projectsubscription',
            name='subscribe_status',
        ),
        migrations.RemoveField(
            model_name='projectsubscription',
            name='user_full_name',
        ),
        migrations.AlterField(
            model_name='periodicprojectupdate',
            name='project_status',
            field=models.CharField(blank=True, choices=[(b'Ongoing', b'Ongoing'), (b'On_Hault', b'On Halt'), (b'Completed', b'Completed')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='projectinfo',
            name='project_status',
            field=models.CharField(blank=True, choices=[(b'Ongoing', b'Ongoing'), (b'On_Hault', b'On Halt'), (b'Completed', b'Completed')], max_length=20, null=True),
        ),
    ]
