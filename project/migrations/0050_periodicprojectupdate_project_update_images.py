# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-07 13:52
from __future__ import unicode_literals

from django.db import migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0049_remove_periodicprojectupdate_budget_spend'),
    ]

    operations = [
        migrations.AddField(
            model_name='periodicprojectupdate',
            name='project_update_images',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='images'),
        ),
    ]
