# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-18 23:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0028_auto_20160918_2338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventimage',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='event_images', to='project.Event'),
        ),
    ]
