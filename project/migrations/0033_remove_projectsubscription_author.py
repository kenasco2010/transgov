# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-24 12:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0032_projectsubscription_subscribe_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectsubscription',
            name='author',
        ),
    ]
