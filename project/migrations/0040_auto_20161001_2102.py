# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-01 21:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0039_auto_20160929_2007'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bulksms',
            old_name='phone_number',
            new_name='phone_numbers',
        ),
    ]
