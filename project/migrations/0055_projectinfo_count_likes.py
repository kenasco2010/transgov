# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-01-16 12:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0054_auto_20170116_1050'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectinfo',
            name='count_likes',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
