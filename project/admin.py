from django.contrib import admin
from models import *
from rest_api.models import *
from company_profile.models import CompanyProfile
from field_worker.models import FieldWorker
from assign_faults.models import AssignFault
from fault_comments.models import FaultComment
# Register your models here.

# class projectAdmin(admin.ModelAdmin):
#     fieldsets = [
#             ('project Details',{'fields':['name','description','amount_allocated',
#                                           'author','start_date','end_date','completeness',
#                                           'time_created','location','img_url','company']})
#
#             ]
#     #readonly_fields =  ('',)
    # list_display =  ('name','description','start_date','end_date')

#
# class ProjectInfo(admin.ModelAdmin):
#     model = ProjectInfo
#     list_display = ( 'image_tag' )
#     readonly_fields = ('image_tag',)

class ProjectInfoInline(admin.TabularInline):
    model = ProjectImage
    extra = 3
    show_change_link = True


class ProjectInfoAdmin(admin.ModelAdmin):
    inlines = [ProjectInfoInline]
    # list_display = ( 'project_image' )


class EventInline(admin.TabularInline):
    model = EventImage
    extra = 5
    show_change_link = True


class EventAdmin(admin.ModelAdmin):
    inlines = [EventInline]


class ProjectImageAdmin(admin.ModelAdmin):
    model = ProjectImage
    list_display = ('image_tag', 'project' )


class FaultReportStatusAdmin(admin.ModelAdmin):
    model = FaultReportStatus
    list_display =  ('cases', 'report_status', 'status_change_time')


myBackendContentModels = [ProjectComment, BulkSMS, IVR,
                          PeriodicProjectUpdate, ProjectSubscription, EventImage,
                          Profile, ProjectCase, CaseCategory, CaseImage, Firebase,
                          Region, Suburb, News, CompanyProfile, FieldWorker, AssignFault,
                          FaultComment]  # iterable list
admin.site.register(myBackendContentModels)
admin.site.register(ProjectInfo, ProjectInfoAdmin)
admin.site.register(ProjectImage, ProjectImageAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(FaultReportStatus, FaultReportStatusAdmin)
