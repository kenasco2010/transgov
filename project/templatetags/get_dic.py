from django import template

register = template.Library()


def get_dic(dic, key):
    try:
        return dic[key]
    except KeyError:
        return None

register.filter('get_dic', get_dic)




