
# RELEVANCE_CHOICES = (
#     (1, ("Unread")),
#     (2, ("Read"))
# )
CATEGORY = (
    ("school", "Schools"),
    ("roads and highways", "Roads and highways"),
    ("hospitals", "Hospitals"),
    ("housing", "Housing"),
    ("toilet facilities", "Toilet facilities"),
    ("borehole", "Borehole"),

)
SUBMETROS = (
    ("Ablekuma Central Sub Metro", "Ablekuma Central Sub Metro"),
    ("Ablekuma North Sub Metro", "Ablekuma North Sub Metro"),
    ("Ablekuma South Sub Metro", "Ablekuma South Sub Metro"),
    ("Ashiedu Keteke Sub Metro", "Ashiedu Keteke Sub Metro"),
    ("Ayawaso Central Sub Metro", "Ayawaso Central Sub Metro"),
    ("Ayawaso East Sub Metro", "Ayawaso East Sub Metro"),
    ("Ayawaso West Sub Metro", "Ayawaso West Sub Metro"),
    ("Okaikoi North Sub Metro", "Okaikoi North Sub Metro"),
    ("Okaikoi South Sub Metro", "Okaikoi South Sub Metro"),
    ("Osu Klottey Sub Metro", "Osu Klottey Sub Metro"),
)
REGIONS = (
    ("Ashanti Region", "Ashanti Region"),
    ("Brong Ahafo Region", "Brong Ahafo Region"),
    ("Central Region", "Central Region"),
    ("Eastern Region", "Eastern Region"),
    ("Greater Accra", "Greater Accra"),
    ("Northern Region", "Northern Region"),
    ("Upper East Region", "Upper East Region"),
    ("Upper West Region", "Upper West Region"),
    ("Volta Region", "Volta Region"),
    ("Western Region", "Western Region"),



)
PROJECT_STATUS = (
   ("Ongoing", "Ongoing"),
    ("On Halt", "On Halt"),
    ("Completed", "Completed")
)

PERIODIC_PROJECT_STATUS = (
    ("Ongoing", "Ongoing"),
    ("On_Hault", "On Halt"),
    ("Completed", "Completed")
)

# NORMAL_USER_APPROVE_COURSE = (
#     ("Pending Approval", "Pending Approval")
# )


