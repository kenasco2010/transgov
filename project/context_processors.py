from rest_api.models import Profile
from company_profile.models import CompanyProfile
def get_profile_info(request):
    try:
        if request.user.is_authenticated():
            user = request.user
            get_profile = Profile.objects.get(user=user)
            get_img = get_profile.profile_image
            return {'profile_image': get_img}
        else:
            return {'default_profile_image': 'https://s3.amazonaws.com/transgovgh-production/profile_image/avatar.png'}
    except Profile.DoesNotExist:
        # present a form asking if you would like to complete your profile after login(that is if you haven't filled up your profile)
        return {'default_profile_image': 'https://s3.amazonaws.com/transgovgh-production/profile_image/avatar.png'}


def view_company_profile(request):
    try:
        if request.user.is_authenticated():
            user = request.user
            get_company_profile = CompanyProfile.objects.get(user=user)
            return {'company_profile': get_company_profile}
        else:
            return {'default_profile_image': 'https://s3.amazonaws.com/transgovgh-production/profile_image/avatar.png'}
    except CompanyProfile.DoesNotExist:
        return {'default_profile_image': 'https://s3.amazonaws.com/transgovgh-production/profile_image/avatar.png'}
