apiHandler = {};

apiHandler.requestProcessor = function(url, method, data, callback) {
	var data = null;

	var xhr = new XMLHttpRequest();
	method = method.toLocaleUpperCase();
	data = (data) ? data : {};
	xhr.withCredentials = true;

	xhr.addEventListener("readystatechange", function() {
		if (this.readyState === 4) {
			callback(JSON.parse(this.responseText));
		}
	});

	xhr.open(method, url);
	xhr.setRequestHeader("cache-control", "no-cache");

	xhr.send(data);
}

  $( function() {
    $( "#start_datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
    $( "#end_datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

