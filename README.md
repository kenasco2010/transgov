# TRANSGOV
This is the official doc for the Transgov project
Please add any special notes concerning the project here

 - reindent.py is included to help format py code
 - main folder is app for entities that are not standalone eg. the end users

### Version
Python 2.7.10
Django 1.9.5

### Installation
Run the following commands after cloning the project:

```sh
$ virtualenv transgovenv
$ source transgovenv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```
## NB:
Remove all py binaries before push (pyc, pyw)
Freeze requirements


### TODO

- Coding style guide
- Each App Description
-


License
----

TRANSGOV copyright 2016
