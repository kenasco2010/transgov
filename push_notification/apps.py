from __future__ import unicode_literals

from django.apps import AppConfig


class PushNotificationConfig(AppConfig):
    name = 'push_notification'
