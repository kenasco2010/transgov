from django.shortcuts import render
from gov import credInfo
from rest_api.notifications import get_firebase
import requests
import json
# Create your views here.

# send general message to mobile app users
def send_push_notification(request):
	fcm_ids = get_firebase()
	context = {
	'all_fcm_ids': fcm_ids
	}
	return render(request, 'push_notification/send_mobile_notification.html', context)
	