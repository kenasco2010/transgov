from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.contrib.auth.models import User
from django.utils import timezone
from imagekit.models import ProcessedImageField
from django.conf import settings
from rest_api.models import ProjectCase, Profile
import sys

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class FaultComment(models.Model):
	fault = models.ForeignKey(ProjectCase, related_name='comments', blank=True, null=True)
	user = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
	comment = models.TextField()
	comment_image = ProcessedImageField(upload_to='comment_images',
                                        format='JPEG',
                                        options={'quality': 60},
                                        null=True, blank=True)
	is_public = models.BooleanField(default=True)
	is_removed = models.BooleanField(default=False)
	date_commented = models.DateTimeField(default=timezone.now)
	modified = models.DateTimeField(auto_now=True)

	def publish(self):
		self.date_commented = timezone.now()
		self.save()

	def __unicode__(self):
		return self.fault.case_name

	def __str__(self):
		return self.fault.case_name

	class Meta:
		verbose_name_plural = "Fault Comments"

	@property
	def get_user_info(self):
		return self.user.username

	@property
	def get_user_profile(self):
		userid = self.user.id
		profile_info = Profile.objects.get(user=userid)
		return profile_info

	@property
	def profile_picture(self):
		userid = self.user.id
		image = Profile.objects.get(user=userid).profile_image
		if not image:
			return None
		else:
			return image.url