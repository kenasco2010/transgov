from __future__ import unicode_literals

from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from imagekit.models import ProcessedImageField
# from geoposition.fields import GeopositionField

from rest_api.choices import CASE_STATUS, SUBURBS

from django.db import models
# from vote.models import FaultVote

import uuid
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# function to generate random numbers for case Number
def generate_random_case_num():
    random_gen = str(uuid.uuid4().get_hex().upper()[0:10])
    return random_gen


# Create your models here.
class Region(models.Model):
  region = models.CharField(blank=True, max_length=70)
  author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
  created_date = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)

  def publish(self):
      self.created_date = timezone.now()
      self.save()

  def __unicode__(self):
      return self.region

  def __str__(self):
      return self.region

  class Meta:
      verbose_name_plural = "Regions"


class Suburb(models.Model):
  region = models.ForeignKey(Region, related_name="regions")
  suburb = models.CharField(blank=True, max_length=70)
  author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
  created_date = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)

  def publish(self):
      self.created_date = timezone.now()
      self.save()

  def __unicode__(self):
      return self.suburb

  def __str__(self):
      return self.suburb
  class Meta:
      verbose_name_plural = "Suburbs"


# fcm_id field is to create notification for the user.
class Firebase(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    fcm_id = models.CharField(max_length=2000, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = "Firebase"


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    region = models.ForeignKey(Region, related_name="profile_regions", blank=True)
    suburb = models.ForeignKey(Suburb, related_name="profile_suburbs", blank=True)  # Location of the person
    phone_number = models.CharField(max_length=20, blank=True)  # Phone number of the user
    profile_image = ProcessedImageField(upload_to='profile_image',
                                        format='JPEG',
                                        options={'quality': 30},
                                           blank=True)
    social_profile_image = models.CharField(max_length=900, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
          self.created_date = timezone.now()
          self.save()

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username


# This is the model for case category
class CaseCategory(models.Model):
    category_name = models.CharField(max_length=40)
    description = models.TextField(blank=True)
    category_image = ProcessedImageField(upload_to='case_category_images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.category_name

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name_plural = "Fault Category"


def convert_category_to_tuple():
        my_set = ()
        # keys = ['id', 'category']
        categories = CaseCategory.objects.all()
        id_list = categories.values_list('id','category_name')
        # new_list = id_list[::1]
        # my_set = set(new_list)
        # new_set = zip(my_set, keys)
        return id_list

# model for project case recording
class ProjectCase(models.Model):
    case_number = models.CharField(max_length=10, blank=True,
                                   null=True, default=generate_random_case_num)
    case_category = models.ForeignKey(CaseCategory, related_name='case_category')
    case_name = models.CharField(max_length=400)
    case_description = models.TextField()
    case_image_one = ProcessedImageField(upload_to='case_image_one_folder',
                                        format='JPEG',
                                        options={'quality': 30},
                                        blank=True)
    case_image_two = ProcessedImageField(upload_to='case_image_two_folder',
                                        format='JPEG',
                                        options={'quality': 30},
                                         blank=True)
    case_image_three = ProcessedImageField(upload_to='case_image_three_folder',
                                        format='JPEG',
                                        options={'quality': 30},
                                           blank=True)
    region = models.ForeignKey(Region, related_name="fault_regions",
                                        blank=True, null=True)
    suburb = models.ForeignKey(Suburb, related_name="fault_suburbs",
                                        blank=True, null=True)
    land_mark = models.TextField(blank=True)
    case_status = models.CharField(choices=CASE_STATUS,
                                    max_length=20, blank=True,
                                    default="pending")
    gravity = models.BooleanField(default=False)
    map_land_mark_name = models.CharField(max_length=500, blank=True)
    latitude = models.CharField(max_length=400, blank=True)
    longitude = models.CharField(max_length=400, blank=True)
    count_gravity = models.IntegerField(blank=True, null=True, default=0)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.case_name

    def __str__(self):
        return self.case_name

    @property
    def public_comments(self): # filters comments and shows only ones allowed for public
        return self.comments.filter(is_public=True)

    @property
    def count_comment_per_post(self): # counts comments on a fault
        return self.comments.filter(is_public=True).count()

    @property 
    def count_votes_per_post(self): # counts votes on a particular fault
        return self.votes.all().count()

    @property
    def vote_user(self): # returns the user who has voted to fault list
        # userId = self.author.id
        fault = self
        return self.votes.filter(fault=fault)
        
        
    class Meta:
        verbose_name_plural = "Community Fault Reports"

class CaseImage(models.Model):
    project_case = models.ForeignKey(ProjectCase, related_name='case_images')
    case_images = ProcessedImageField(upload_to='additional_case_images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    author = models.ForeignKey(AUTH_USER_MODEL,  on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date =timezone.now()
        self.save()

    def __unicode__(self):
        return self.project_case.case_name

    def __str__(self):
        return self.project_case.case_name



# Model for logs of reports coming in.
class FaultReportStatus(models.Model):
    cases = models.ForeignKey(ProjectCase, related_name='reported_case_status', blank=True, null=True)
    report_status = models.CharField(max_length=20, blank=True)
    status_change_time = models.DateTimeField()
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.cases.case_name

    def __str__(self):
        return self.cases.case_name


    class Meta:
        verbose_name_plural = "Fault reported status"