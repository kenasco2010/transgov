# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-02-06 16:49
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import imagekit.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CaseCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_name', models.CharField(max_length=40)),
                ('description', models.TextField(blank=True, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location', models.CharField(blank=True, choices=[(b'Ablekuma Central Sub Metro', b'Ablekuma Central Sub Metro'), (b'Ablekuma North Sub Metro', b'Ablekuma North Sub Metro'), (b'Ablekuma South Sub Metro', b'Ablekuma South Sub Metro'), (b'Ashiedu Keteke Sub Metro', b'Ashiedu Keteke Sub Metro'), (b'Ayawaso Central Sub Metro', b'Ayawaso Central Sub Metro'), (b'Ayawaso East Sub Metro', b'Ayawaso East Sub Metro'), (b'Ayawaso West Sub Metro', b'Ayawaso West Sub Metro'), (b'Okaikoi North Sub Metro', b'Okaikoi North Sub Metro'), (b'Okaikoi South Sub Metro', b'Okaikoi South Sub Metro'), (b'Osu Klottey Sub Metro', b'Osu Klottey Sub Metro')], max_length=40, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectCase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('case_number', models.CharField(blank=True, max_length=10, null=True)),
                ('case_name', models.CharField(max_length=400)),
                ('case_description', models.TextField()),
                ('case_image', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='images')),
                ('location', models.CharField(blank=True, max_length=50, null=True)),
                ('case_status', models.CharField(blank=True, choices=[(b'pending', b'Pending'), (b'fixing', b'Fixing'), (b'case_fixed', b'Cased fixed')], default='Pending', max_length=20, null=True)),
                ('gravity', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('case_category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='case_category', to='rest_api.CaseCategory')),
            ],
        ),
    ]
