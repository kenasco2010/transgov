from django.conf import settings
from django.conf.urls import patterns, url, include
from django.contrib import admin
admin.autodiscover()

from rest_framework.routers import DefaultRouter

from rest_api.views import *
from transgov_partners.views import * 
from assign_faults.api_views import AssignFaultViewSet


router = DefaultRouter()
router.register(r'v1.0/regions', RegionViewSet)
router.register(r'v1.0/suburbs', SuburbViewSet)
router.register(r'v1.0/all-projects', ProjectInfoViewSet)
router.register(r'v1.0/project-images', ProjectImagesViewSet)
router.register(r'v1.0/events', EventViewSet)
router.register(r'v1.0/users', UserViewSet)
router.register(r'v1.0/profile', ProfileViewSet)
router.register(r'v1.0/case-category', CaseCategoryViewSet)
router.register(r'v1.0/project-case', ProjectCaseViewSet)
router.register(r'v1.0/project-case-status', FaultReportStatusViewSet)

router.register(r'v1.0/assign-faults', AssignFaultViewSet)
router.register(r'v1.0/field-workers', FieldWorkersViewSet)


# router.register(r'v2.0/project_case', ProjectCaseViewSet_v2)

# paginated urls (version 2)
router.register(r'v2.0/all-projects', ProjectInfoViewSet_v2)


urlpatterns = [
    url(r'^', include(router.urls)),
]
