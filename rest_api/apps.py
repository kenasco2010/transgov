from __future__ import unicode_literals

from django.apps import AppConfig

from django.apps import AppConfig
import algoliasearch_django as algoliasearch

class RestApiConfig(AppConfig):
    name = 'rest_api'

    def ready(self):
        ProjectCase = self.get_model('projectcase')
        algoliasearch.register(ProjectCase)

  