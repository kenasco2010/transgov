from rest_framework import serializers
from project.models import ProjectInfo, ProjectImage, Event
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponseForbidden
from rest_framework.decorators import (api_view, permission_classes, list_route)
from django.core.validators import validate_email
from rest_framework.serializers import ModelSerializer
from rest_api.models import Profile, ProjectCase, CaseCategory, \
    CaseImage, Firebase, Region, Suburb, FaultReportStatus
from fault_comments.models import FaultComment


from field_worker.models import FieldWorker
from assign_faults.models import AssignFault
from vote.models import FaultVote


class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Region

        fields = ('id', 'region', 'author', 'created_date', 'modified')


class SuburbSerializer(serializers.ModelSerializer):
    region = RegionSerializer()
    class Meta:
        model = Suburb

        fields = ('id', 'region', 'suburb', 'author', 'created_date', 'modified')

class ProjectImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectImage

        fields = ('id', 'project', 'additional_project_images')


class ProjectInfoSerializer(serializers.ModelSerializer):
    projectImages = ProjectImagesSerializer(many=True)
    def set_up_eager_loading(cls, queryset):
        queryset = queryset.prefetch_related('projectImages')
        return queryset

    class Meta:
        model = ProjectInfo
        fields = ('id', 'project_name', 'project_description', 'amount_allocated',
                  'estimated_budget_GOG', 'estimated_budget_IGF', 'estimated_budget_Donor',
                  'actual_Budget_Spend', 'project_contact_person', 'project_contractor',
                  'project_status', 'project_category', 'start_date', 'end_date',
                  'completeness', 'project_location', 'regions', 'sub_metro', 'position',
                  'project_image', 'projectImages', 'count_likes', 'published_date', 'modified'
                  )


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'event_name', 'event_description', 'start_date',
                  'event_time', 'venue','region', 'sub_metro', 'position',
                  'event_image','published_date', 'modified'
                  )


class CaseImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = CaseImage
        fields = ('id', 'case_images')


class CaseCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = CaseCategory
        fields = ('id', 'category_name', 'description', 'category_image', 'author')


class FaultReportStatusRefSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = FaultReportStatus
        fields = ('id', 'cases', 'report_status', 'status_change_time', 
                'created_date', 'modified')


class FaultCommentSerializer(serializers.ModelSerializer): 
    username = serializers.ReadOnlyField(source='get_user_info')
    first_name = serializers.ReadOnlyField(source = 'get_user_profile.first_name')
    last_name = serializers.ReadOnlyField(source = 'get_user_profile.last_name')
    profile_image = serializers.ReadOnlyField(source = 'profile_picture')
    class Meta:
        model = FaultComment
        fields = ('id', 'fault', 'user','username', 'first_name', 'last_name', 
             'profile_image', 'comment', 'comment_image', 'is_public', 'is_removed', 'date_commented')


class FaultVoteSerializer(serializers.ModelSerializer): 

    class Meta:
        model = FaultVote
        fields = ('id', 'fault', 'user')

class VoteMassageSerializer(serializers.ModelSerializer):
    vote_user = serializers.ReadOnlyField()
    class Meta:
        model = FaultVote
        fields = ('id', 'vote_user','user')


class ProjectCaseSerializer(serializers.ModelSerializer):
    comments = FaultCommentSerializer(many=True, source='public_comments')
    comments_count_per_post = serializers.ReadOnlyField(source='count_comment_per_post')
    vote_count = serializers.ReadOnlyField(source='count_votes_per_post')
    voted_user_id = VoteMassageSerializer(many=True, source='vote_user') # This shows a user who has voted,(from faut report model)
    case_category = CaseCategorySerializer()
    region = RegionSerializer()
    suburb = SuburbSerializer()
    reported_case_status = FaultReportStatusRefSerializer(many=True)

    class Meta:
        model = ProjectCase
        fields = ('id', 'case_number', 'case_category', 'case_name',
                  'case_description', 'case_image_one', 'case_image_two',
                  'case_image_three', 'case_status', 'map_land_mark_name', 'latitude', 'longitude',
                  'region', 'suburb', 'land_mark', 'author', 'gravity',
                  'count_gravity', 'created_date', 'reported_case_status', 'comments', 'comments_count_per_post',
                  'vote_count', 'voted_user_id'
                  )


# This field worker serializer is added to the login serializer
class FieldWorkerSerializer(serializers.ModelSerializer):
    suburb = SuburbSerializer()

    class Meta:
        model = FieldWorker
        fields = ('id', 'full_name', 'company', 'suburb', 'phone_number', 
            'field_worker_image')


class UserSerializer(serializers.ModelSerializer):

    auth_token = serializers.CharField(max_length=500, read_only=True)
    # email = serializers.EmailField(max_length=None, min_length=None, allow_blank=False)

    class Meta:
        model = get_user_model()
        fields = ('id', 'auth_token', 'email', 'username')




class ProfileSerializer(serializers.ModelSerializer):
    region = RegionSerializer()
    suburb = SuburbSerializer()

    class Meta:
        model = Profile
        fields = ('id', 'region', 'suburb', 'first_name', 'last_name', 'phone_number', 'profile_image', 'social_profile_image')


class UserAuthSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(max_length=500, read_only=True)
    email = serializers.EmailField()
    profile = ProfileSerializer()
    fieldworker = FieldWorkerSerializer()

    def validate_email(self, value):
        if not validate_email(value):
            raise serializers.ValidationError("Not a valid email.")
        return value

    class Meta:
        model = get_user_model()
        fields = ('id', 'auth_token', 'email', 'username',
                  'profile', 'fieldworker', 'firebase')


class FirebaseIdSerializer(serializers.ModelSerializer):
    user = UserAuthSerializer

    class Meta:
        model = Firebase
        fields =('id', 'fcm_id', 'user')


class CaseRefSerializer(serializers.ModelSerializer):
    # case_images = CaseImageSerializer(many=True)
    case_category = CaseCategorySerializer()
    region = RegionSerializer()
    suburb = SuburbSerializer()

    class Meta:
        model = ProjectCase
        fields = ('id', 'case_number', 'case_category', 'case_name',
                 'case_status', 'map_land_mark_name',
                  'region', 'suburb', 'land_mark', 'author', 'gravity',
                  'count_gravity', 'created_date')

class FaultReportStatusSerializer(serializers.ModelSerializer):
    cases = CaseRefSerializer()
    class Meta:
        model = FaultReportStatus
        fields = ('id', 'cases', 'report_status', 'status_change_time', 
                'created_date', 'modified')



class AssignFaultSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssignFault
        fields = (
            'community_fault', 'field_worker', 'date_assigned'
            )


class AssignFaultToFieldWorkerSerializer(serializers.ModelSerializer):
    fault_id = serializers.ReadOnlyField(source='community_fault.id')
    fault_description = serializers.ReadOnlyField(source='community_fault.case_description')
    fault_region = serializers.ReadOnlyField(source='region')
    fault_suburb = serializers.ReadOnlyField(source='suburb')
    fault_land_mark = serializers.ReadOnlyField(source='community_fault.land_mark')

    field_worker_id = serializers.ReadOnlyField(source='id')

    field_worker = serializers.SlugRelatedField(
        read_only=True,
        slug_field='full_name')

    community_fault = serializers.SlugRelatedField(
        read_only=True,
        slug_field='case_name'
    )


    class Meta:
        model = AssignFault
        fields = (
            'fault_id','community_fault', 'fault_description', 'fault_region', 'fault_suburb',
            'fault_land_mark', 'field_worker_id', 'field_worker', 'date_assigned'
            )


class FieldworkerSerializer(serializers.ModelSerializer):
    # assigned_field_worker = AssignFaultSerializer()

    class Meta:
        model = FieldWorker
        fields = (
            'user', 'full_name', 'company', 'suburb', 'phone_number',
            'field_worker_image'
            )




class AddFaultCommentSerializer(serializers.ModelSerializer): 

    class Meta:
        model = FaultComment
        fields = ('id', 'fault', 'user', 'comment', 'comment_image', 'is_public', 'is_removed', 'date_commented')

        