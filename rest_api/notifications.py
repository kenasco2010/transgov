from rest_api.models import Firebase
import requests
import json

def get_firebase():
    firebase = Firebase.objects.values_list('fcm_id', flat=True)
    fcm_idStrip = map(lambda x: x.encode('ascii'), firebase)
    fcm_idToJson = json.dumps(fcm_idStrip)
    return fcm_idStrip


