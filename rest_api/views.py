# from django.shortcuts import render
import os
from django.contrib.auth import authenticate, login, logout, get_user_model
# from django.contrib.auth.models import User

# Django send emails
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import pre_save, post_save

from django.utils import timezone

from rest_framework import viewsets
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_api.serializers import *
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import FileUploadParser


import re
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view

from rest_api.permissions import IsOwner
from rest_api.models import Profile, ProjectCase, CaseCategory, \
                            CaseImage, Firebase, Region, Suburb, \
                            Firebase, FaultReportStatus
from project.models import *
from vote.models import FaultVote
from gov import credInfo
from rest_api.notifications import get_firebase
import requests
import json
import time


# override pagination class
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 20


class RegionViewSet(viewsets.ModelViewSet):
    model = Region
    serializer_class = RegionSerializer
    queryset = Region.objects.all()
    permission_classes = (AllowAny,)


class SuburbViewSet(viewsets.ModelViewSet):
    model = Suburb
    serializer_class = SuburbSerializer
    queryset = Suburb.objects.all()
    permission_classes = (AllowAny,)


class ProjectInfoViewSet(viewsets.ModelViewSet):
    model = ProjectInfo
    serializer_class = ProjectInfoSerializer
    queryset = ProjectInfo.objects.all()
    permission_classes = (AllowAny,)


    """
        Adds likes to project. (rewrite this code.)
    """
    @detail_route(methods=('GET',),)
    def like(self, request, pk=None, *args, **kwargs):
        user = request.user.is_authenticated()
        project = self.get_object()
        if user and project.like_success != True:
            project.like_success = True
            project.count_likes += 1
            project.save()

        else:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                             "message": "Request not sent",
                             'success': False,
                             'likes': project.count_likes
                             },
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {
                'likes': project.count_likes,
                'success': True
            },
            status=HTTP_201_CREATED
        )

    """
        Gets all projects with it's images.

    """

class ProjectImagesViewSet(viewsets.ModelViewSet):
    model = ProjectImage
    serializer_class = ProjectImagesSerializer
    queryset = ProjectImage.objects.all().order_by('id')
    permission_classes = (AllowAny,)

    @detail_route(methods=['get'])
    def project(self, request, *args, **kwargs):

        """
        Get all images of a particular project

        """
        module = self.get_object()
        myqueryset = module.project.all().order_by('-timestamp')
        serializer = ProjectInfoSerializer(myqueryset, context={'request': request}, many=True)
        return Response({'results': serializer.data}, status=HTTP_200_OK)


    """
        Get's all events uploaded on the dashboard
    """

class EventViewSet(viewsets.ModelViewSet):
    model = Event
    serializer_class = EventSerializer
    queryset = Event.objects.all()
    permission_classes = (AllowAny,)



    """
        This allows users to sign up, login, logout
    """
class UserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsOwner, )
    serializer_class = UserAuthSerializer
    queryset = User.objects.all()

    # list users but only logged in user's data will show because of (IsOwner)
    def list(self, request, *arg, **kwargs):
        users = [request.user]
        users = self.serializer_class(users, many=True)
        return Response(users.data)

    # viewset to create user, anyone can access this view
    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def signup(self, request, *args, **kwargs):
        """
        Allows a user to signup to the platform using email
        POST PARAMETERS:
        userName = username of the user
        firstName = username of the user
        lastName = username of the user
        email = Email of User
        password = Password of User
        """
        email = request.data.get('email', None)
        username = request.data.get('username', None)
        password = request.data.get('password', None)

        # checks if the input in the email field is valid with regex (re)
        validate_email = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
        # check if username and email is not empty
        if not username:
            return Response({'status_code':status.HTTP_401_UNAUTHORIZED,
                            'message': 'please enter username'
                            })
        if not email or not validate_email:
            return Response({'status_code':status.HTTP_401_UNAUTHORIZED,
                            'message': 'please enter a valid email address'
                            })

        # This checks if the username and email already exist.
        if User.objects.filter(username=username).exists():
            return Response({'status_code':status.HTTP_401_UNAUTHORIZED,
                            'message': 'Username already exist, please use different username'
                            })
        if User.objects.filter(email=email).exists():
            return Response({'status_code':status.HTTP_401_UNAUTHORIZED,
                            'message': 'email already exist, please use different email'
                            })

        if email is not None and username is not None\
            and password is not None:

            mymodel = get_user_model()
            user = mymodel.objects.create(
                email=email,
                username=username,
            )

            user.set_password(password)
            user.save()
            user = authenticate(username=user.username,  password=password)
            login(request, user)
            user = UserAuthSerializer(user)
            return Response({'status_code': status.HTTP_201_CREATED,
                            'message': "You have successfully signed up",
                            'result':{
                            'data':user.data
                            },
                        })
        else:
            return Response({'status_code':status.HTTP_400_BAD_REQUEST,
                            'message': 'Please supply required parameters'
                            })

    # update user fields(first_name & last_name) after Account registration
    @list_route(methods=('POST',),  url_path="update-user-firstname-lastname",  permission_classes=(IsOwner,))
    def update_account_fn_ln(self, request, *args, **kwargs):
        user = request.user
        user.first_name = request.data.get('first_name')
        user.last_name = request.data.get('last_name')
        user.save()
        user = UserAuthSerializer(user)
        return Response({"status_code": status.HTTP_201_CREATED,
                         "message": "Successfully added first name and last name",
                         "result":{
                            "data":user.data
                         },
                         })

    @list_route(methods=('post',), url_path="add-profile")
    def add_user_profile(self, request, *args, **kwargs):
        # user = Profile.objects.get(user = request.user)
        user = request.user
        regions = Region.objects.get(pk=request.data.get("region"))
        suburbs = Suburb.objects.get(pk=request.data.get("suburb"))
        try:
            get_user_profile = Profile.objects.get(user=request.user)
            get_user_profile.first_name = request.data.get("first_name")
            get_user_profile.last_name = request.data.get("last_name")
            get_user_profile.region = regions
            get_user_profile.suburb = suburbs
            get_user_profile.phone_number = request.data.get("phone_number")
            get_user_profile.profile_image = request.FILES.get("profile_image")
            get_user_profile.social_profile_image = request.data.get("social_profile_image")
            get_user_profile.save()
            user = self.serializer_class(user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "You have updated your profile details",
                             "result": {
                             "data": user.data
                             },
                             })
        except Profile.DoesNotExist:
            Profile.objects.create(
                user=user,
                first_name= request.data.get("first_name"),
                last_name = request.data.get("last_name"),
                region=regions,
                suburb=suburbs,
                phone_number=request.data.get("phone_number"),
                profile_image=request.FILES.get("profile_image"),
                social_profile_image = request.data.get("social_profile_image")
            )
            user = self.serializer_class(user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "You have added your location and phone number",
                             "result": {
                             "data":user.data
                                },
                             })

    @detail_route(methods=('GET',), url_path='user-profile')
    def get_user_profile(self, request, pk=None, *args, **kwargs):
        user = self.get_object()
        user_profile = Profile.objects.get(user=user) 
        if user_profile:
            user_profile = ProfileSerializer(user_profile)
            return Response({"status_code": status.HTTP_200_OK,
                                 "message": "Your profile information",
                                 "result": {
                                 "data":user_profile.data
                                    },
                                 })
        else:
            return Response({"status_code": status.HTTP_200_OK,
                             "message": "Please complete your profile"
                             })

    #Allows users to login into the app.
    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def login(self, request, *args, **kwargs):
        """
        Allow users to login using username and password

        POST PARAMETERS:
        username = Typed Username
        password = Of course Password typed
        """
        start_time = time.time()
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                Token.objects.get(user=user).delete()
                Token.objects.create(user=user)
                login(request, user)
                user = UserAuthSerializer(user)
                print "My program took", time.time() - start_time, "to run"
                return Response({'status_code': status.HTTP_200_OK,
                                'message': "You have successfully logged in",
                                'result':{
                                   'data': user.data
                                    },
                                })
            else:
                return Response({'status_code':status.HTTP_404_NOT_FOUND,
                                'message': 'This account has been deactivated'
                                })
        else:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'Invalid username or password'
                            })

    # Allow users to logout
    @detail_route(methods=('POST',))
    def logout(self, request, pk=None, *args, **kwargs):
        """
        Logs out a user
        """
        user = self.get_object()
        try:
            Token.objects.get(user=user).delete()
            Token.objects.create(user=user)
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have been successfully logged out'
                            })
        except:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                            'message': 'Sorry we could not log you out'
                            })

    # Updates fcm when a user signups up on the app.
    @list_route(methods=('POST', ), url_path='update-fcm', permission_classes=(IsOwner, ))
    def update_fcm_id(self, request, *args, **kwargs):
        user = request.user
        try:
            get_user = Firebase.objects.get(user=request.user)
            get_user.fcm_id = request.data.get("fcm_id")
            get_user.save()
            get_user = FirebaseIdSerializer(get_user)
            return Response({
                         "status_code": status.HTTP_200_OK,
                         "message": "Successfully updated Fcm id",
                         "result": get_user.data
                        })
        except Firebase.DoesNotExist:
            firebase_id = Firebase.objects.create(
                user = user,
                fcm_id = request.data.get('fcm_id')
            )
            firebase_id = FirebaseIdSerializer(firebase_id)
            return Response({"status_code": status.HTTP_200_OK,
                             "message": "Successfully added Fcm id",
                             "result": firebase_id.data
                             })


    # Get count of reports a user has made.
    @detail_route(methods=('GET',), url_path="user-activities", permission_classes=(IsOwner,))
    def count_user_fault_report(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            logged_in_user = request.user
            count_report = ProjectCase.objects.filter(author=logged_in_user).count()
            count_pending_report = ProjectCase.objects.filter(
                author=logged_in_user, case_status="pending").count()
            count_fixing_report = ProjectCase.objects.filter(
                author=logged_in_user, case_status="Being fixed").count()
            count_fixed_report = ProjectCase.objects.filter(
                author=logged_in_user, case_status="Fault fixed").count()
            return Response({"status_code": status.HTTP_200_OK,
                            "message": "Number of faults you have reported",
                            "result": {
                            "total_report": count_report,
                            "pending_report": count_pending_report,
                            "fixing_fault": count_fixing_report,
                            "fixed_fault": count_fixed_report
                                }
                            })
        else:
            return Response({"status_code": status.HTTP_204_NO_CONTENT,
                            "message": "log in to see your activities",
                            "result": "No content"
                            })


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes(IsOwner)
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()


class CaseCategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CaseCategorySerializer
    queryset = CaseCategory.objects.all()


class ProjectCaseViewSet(viewsets.ModelViewSet):
    model = ProjectCase
    serializer_class = ProjectCaseSerializer
    queryset = ProjectCase.objects.select_related().order_by('-id')
    # permission_classes(AllowAny, )
    pagination_class = StandardResultsSetPagination

    @list_route(methods=('POST',), url_path='create-case', permission_classes=(IsAuthenticated,))
    def create_project_case(self, request,  *args, **kwargs):
        user = request.user
        get_category = CaseCategory.objects.get(pk=request.data.get("case_category"))
        # regions = Region.objects.get(pk=request.data.get("region"))
        # suburbs = Suburb.objects.get(pk=request.data.get("suburb"))
        if user:

            create_case = ProjectCase.objects.create(
                author=request.user,
                case_category=get_category,
                case_name=request.data.get('case_name'),
                case_description=request.data.get('case_description'),
                map_land_mark_name=request.data.get('map_land_mark_name'),
                latitude=request.data.get('latitude'),
                longitude=request.data.get('longitude'),
                case_image_one=request.FILES.get('case_image_one'),
                case_image_two=request.FILES.get('case_image_two'),
                case_image_three=request.FILES.get('case_image_three'),
                # region=regions,
                # suburb=suburbs,
                land_mark=request.data.get("land_mark")
            )
            create_case = self.serializer_class(create_case)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "You have reported a fault",
                             "results":  create_case.data
                             })


    @detail_route(methods=('POST',), url_path='update-case', permission_classes=(IsAuthenticated, ))
    def update_case(self, request, pk=None, *args, **kwargs):
        user = request.user
        fault = self.get_object()
        user_fault = fault.author
        get_category = CaseCategory.objects.get(pk=request.data.get("case_category"))
        # regions = Region.objects.get(pk=request.data.get("region"))
        # suburbs = Suburb.objects.get(pk=request.data.get("suburb"))
        if user_fault == user:
            fault.case_category = get_category
            fault.case_name = request.data.get("case_name")
            fault.case_description = request.data.get("case_description")
            fault.map_land_mark_name=request.data.get('map_land_mark_name')
            fault.latitude=request.data.get('latitude')
            fault.longitude=request.data.get('longitude')
            fault.case_image_one = request.data.get('case_image_one')
            fault.case_image_two = request.data.get('case_image_two')
            fault.case_image_three = request.data.get('case_image_three')
            # fault.region=regions
            # fault.suburb=suburbs
            fault.land_mark=request.data.get("land_mark")
            fault.save()
            fault = self.serializer_class(fault)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "You have successfully updated the fault reported",
                             "results":  fault.data})
        else:
            return Response({"status_code": status.HTTP_401_UNAUTHORIZED,
                             "message": "Reported fault could not be updated, " 
                             "make sure you have permission to edit this fault"
                             })

    # Endpoint for deleting a fault
    @detail_route(methods=('POST',), url_path='delete-fault')
    def delete_fault_reported(self, request, pk=None, *args, **kwargs):
        user = request.user
        fault = self.get_object()
        if fault.author == user:
            fault.delete()
            return Response({ "status_code": status.HTTP_200_OK,
                              "message": "Fault has been deleted successfully"
                                })
        else:
            return Response({ "status_code": status.HTTP_401_UNAUTHORIZED,
                              "message": "You do not have permission to delete this fault"
                                })

    # Upvoting a fault means the fault should be attended to with immediate effect
    @detail_route(methods=('POST',), url_path='add-vote-to-fault', permission_classes=(IsAuthenticated,))
    def add_vote_to_fault(self, request, *args, **kwargs):
        user = request.user
        fault = self.get_object()
        try:
            voted = FaultVote.objects.get(fault=fault, user=user)
            if voted:
                voted.delete()
                serialize_votes = FaultVoteSerializer(voted)
                return Response({"status_code": status.HTTP_201_CREATED,
                                "message": "You have downvoted this fault reported",
                                "results": serialize_votes.data
                                })
        except FaultVote.DoesNotExist:
            vote = FaultVote.objects.create(
                fault = fault,
                user = user
                )
            serialize_votes = FaultVoteSerializer(vote)
            return Response({"status_code": status.HTTP_201_CREATED,
                                "message": "You have voted this fault reported",
                                "results": serialize_votes.data
                                })


    # This fault images are additional images to upload when you report a fault. (Not being used)
    @detail_route(methods=('POST',), url_path='add-images')
    def add_case_images(self, request, pk=None, *args, **kwargs):
        user = request.user.is_authenticated()
        if user:
            get_project = self.get_object()
            add_image = CaseImage.objects.create(
                author=request.user,
                project_case=get_project,
                case_images=request.data.get('case_images')
            )
            add_image = CaseImageSerializer(add_image)
            return Response(add_image.data, status=HTTP_201_CREATED)
        else:
            return Response({"message": "you don't have permission to view this data"},
                            status=status.HTTP_401_UNAUTHORIZED)


    # add Fault report comments
    @detail_route(methods=('POST',), url_path='add-comment', permission_classes=(IsAuthenticated,))
    def add_comment_to_fault(self, request, pk=None, *args, **kwargs):
        fault = self.get_object()
        comment = FaultComment.objects.create(
            fault = fault,
            user = request.user,
            comment = request.data.get('comment'),
            comment_image = request.FILES.get('comment_image')
            )
        comment = AddFaultCommentSerializer(comment)
        return Response({
                        "status_code": status.HTTP_201_CREATED,
                         "message": "You have commented on this fault.",
                         "results": comment.data
        })

    # http://localhost:8000/api/v1.0/project-case/my-cases/ #get only my cases i created
    @list_route(methods=('GET',), url_path='my-cases', permission_classes=(IsOwner,))
    def my_cases(self, request, *args, **kwargs):
        """
            Gets all cases i have created
        """
        user = request.user.is_authenticated()
        myuser = request.user
        if user:
            get_cases = ProjectCase.objects.filter(author=myuser).order_by('-id')
            get_cases = self.serializer_class(get_cases, many=True)
            return Response({
                             "status_code": status.HTTP_200_OK,
                             "message": "you have gotten all data for pending cases",
                             "results": get_cases.data
                            })
        else:
            return Response({
                            "status_code": status.HTTP_401_UNAUTHORIZED,
                            "message": "you don't have permission to view this data"
                            })

    # http://localhost:8000/api/v1.0/project-case/pending-cases/ #Get pending cases only
    @list_route(methods=('GET',), url_path='pending-cases', permission_classes=(IsOwner,))
    def pending_cases(self, request, *args, **kwargs):
        user = request.user.is_authenticated()
        if user:
            get_cases = ProjectCase.objects.filter(case_status="pending").order_by('-id')
            get_cases = self.serializer_class(get_cases, many=True)
            return Response({
                             "status_code": status.HTTP_200_OK,
                             "message": "you have gotten all data for pending cases",
                             "results": get_cases.data
                            })
        else:
            return Response({
                            "status_code": status.HTTP_401_UNAUTHORIZED,
                             "message": "you don't have permission to view this data"
                            })


    # http://localhost:8000/api/v1.0/project-case/fixing-cases/
    @list_route(methods=('GET',), url_path='fixing-cases')
    def fixing_cases(self, request, *args, **kwargs):
        get_cases = ProjectCase.objects.filter(case_status="Being fixed").order_by('-id')
        get_cases = self.serializer_class(get_cases, many=True)
        return Response({
                         "status_code": status.HTTP_200_OK,
                         "message": "you have gotten all data for fixing cases",
                         "results": get_cases.data
                         })

    # http://localhost:8000/api/v1.0/project-case/fixed-cases/
    @list_route(methods=('GET',), url_path='fixed-cases', permission_classes=(IsOwner,))
    def fixed_cases(self, request, *args, **kwargs):
        # user = request.user.is_authenticated()
        # if user:
        get_cases = ProjectCase.objects.filter(case_status="Fault fixed").order_by('-id')
        get_cases = self.serializer_class(get_cases, many=True)
        return Response({
                         "status_code": status.HTTP_200_OK,
                         "message": "you have gotten all data for fixed cases",
                         "results": get_cases.data
                        })


class ProjectInfoViewSet_v2(viewsets.ModelViewSet):
    permission_classes(AllowAny,)
    queryset = ProjectInfo.objects.all()
    serializer_class = ProjectInfoSerializer
    pagination_class = StandardResultsSetPagination


class FaultReportStatusViewSet(viewsets.ModelViewSet):
    permission_classes(AllowAny,)
    queryset = FaultReportStatus.objects.all()
    serializer_class = FaultReportStatusSerializer
    pagination_class = StandardResultsSetPagination
    


  # Send Email to transGov to when someone reports a new fault
def send_case_info_to_transGov(sender, instance, *args, **kwargs):
    htmly = get_template('sendEmails/send_case_email_to_transgov.html')
    plaintext = get_template('sendEmails/send_case_email_to_transgov.txt')
    d = Context({ 'case_id': instance.id,
                  'case_ref_number': instance.case_number,
                  'case_name': instance.case_name
                  })
    email_subject = 'TransGov Fault Reporting'
    to=credInfo.TRANSGOV_FAULTS_EMAIL
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                                 settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


# # send notification to all mobile device after report
@receiver(post_save, sender=ProjectCase)
def status_changed(sender, instance, created=False, *args, **kwargs):
    # keeps track of the time fault report status changes
    obj = sender.objects.get(pk=instance.pk)
    FaultReportStatus.objects.create(
        cases=obj,
        report_status=obj.case_status,
        status_change_time=timezone.now()
        )
    if created:
        obj = sender.objects.get(pk=instance.pk)
        send_case_info_to_transGov(sender, instance)

        # Firebase send notification to all when a new fault is reported
        url = 'http://fcm.googleapis.com/fcm/send'
        headers = {'Authorization': credInfo.FIREBASE_KEY}
        fcm_id = get_firebase()
        payload = { 
                    "data":{
                      "id":obj.id,
                       "title" : obj.case_name,
                        "body" : obj.case_description,
                        "click_action" : "org.transgovgh.app.FaultDetailActivity",
                          "type":"fault"
                    },
                    "registration_ids": fcm_id
                }
        r = requests.post(url, headers=headers, json = payload)
    else:
        try:
            report_user = instance.author
            report_user_fcm = Firebase.objects.get(user=report_user)
            
            # Firebase send notification to all when a new fault is updated
            url = 'http://fcm.googleapis.com/fcm/send'
            headers = {'Authorization':credInfo.FIREBASE_KEY}
            payload = {
                        "data": {
                        "id": instance.id,
                        "click_action":"org.transgovgh.app.FaultDetailActivity",
                        "title":instance.case_name,
                        "message":"The fault you reported has been updated to "
                         + instance.case_status + ". Thank you for reporting using TransGov"
                        },
                      "to" : report_user_fcm.fcm_id
                    }
            r = requests.post(url, headers=headers, json = payload)
        except Firebase.DoesNotExist:
            pass
           
    
    
        

class FieldWorkersViewSet(viewsets.ModelViewSet):
    # permission_classes(AllowAny,)
    queryset = FieldWorker.objects.all()
    serializer_class = FieldworkerSerializer
    pagination_class = StandardResultsSetPagination

    @list_route(methods=('GET',), url_path='faults', permission_classes= (IsOwner,))
    def get_faults_assigned_to_field_worker(self, request, *args, **kwargs):
        current_user = request.user
        try:
            field_worker = FieldWorker.objects.get(user=current_user)
            faults = AssignFault.objects.filter(field_worker=field_worker)
            if faults:
                faults = AssignFaultToFieldWorkerSerializer(faults, many=True)
                return Response({
                    "status_code": status.HTTP_200_OK,
                     "message": "All faults assigned to field worker",
                     "result": faults.data
                    })
            else:
                return Response({
                    "status_code": status.HTTP_204_NO_CONTENT,
                     "message": "No fault has been assigned to you"
                    })
        except FieldWorker.DoesNotExist:
             return Response({
                    "status_code": status.HTTP_404_NOT_FOUND,
                     "message": "Field worker does not exist"
                    })

        

