from django.conf.urls import url
import views

urlpatterns = [
    url(r'^account/new/$', views.add, name='add_account'),
    url(r'^account/(?P<user_type>[\w\-]+)/$', views.view, name='view_account'),
    url(r'^account/staff/(\d+)/edit$', views.edit, name='staff_edit_account'),
    url(r'^account/subscribers/(\d+)/edit$', views.edit, name='subscribers_edit_account'),
    url(r'^account/staff/(\d+)/delete$', views.delete, name='staff_delete_account'),
    url(r'^account/subscribers/(\d+)/delete$', views.delete, name='subscribers_delete_account'),
    url(r'^account/(?P<user_type>[\w\-]+)/$', views.view, name='subscriber'),
    url(r'^profile$', views.profile, name='profile'),
   ]
