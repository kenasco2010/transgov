from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import render
from cerberus import *
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from register.models import userMeta
from validate_email import validate_email
from helpers.sms import send_sms

import random

# Create your views here.

@login_required
def view(request, user_type):
    if request.user.is_authenticated() and request.user.is_superuser:
        if user_type == 'staff':
            users = User.objects.filter(is_staff=1, is_superuser=0).values()
        elif user_type == 'subscribers':
            users = User.objects.filter(is_staff=0, is_superuser=0).values()
        else:
            HttpResponseRedirect('/admin/')
        context = {
          'users': users
        }
    else:
        return HttpResponseRedirect('/project/')
    return render(request, 'viewAccount.html', context)

@login_required
def add(request):

    if request.user.is_authenticated() and request.user.is_superuser:

            context = {

            }

            context = {}
            if request.method == 'POST':
                args = {}
                errors = []
                info = []
                for element in request.POST:
                    args[element] = request.POST[element]
                email_is_valid = validate_email(args['email'])
                if email_is_valid:

                    validationSchema = {'username': {'required': True, 'type': 'string',
                                                     'maxlength': 10, 'minlength': 4},
                                        'number': {'required': True, 'type': 'number', 'maxlength': 10},
                                        'first_name': {'required': True, 'type': 'string', 'minlength': 1},
                                        'last_name': {'required': True, 'type': 'string', 'minlength': 1}
                                        }
                    fields = {
                        'username': args['username'],
                        'number': int(args['number']),
                        'first_name': args['first_name'],
                        'last_name': args['last_name']

                    }
                    v = Validator()
                    if v.validate(fields, validationSchema) == True:
                        users = User.objects.all()
                        try:
                            errors.append('Username or Email already exist')
                            user = User.objects.get(username=args['username'])
                            user = User.objects.get(email=args['email'])
                        except User.DoesNotExist:
                            if int(args['type']) == 0:
                                args['is_active'] = 1
                                args['is_staff'] = 1
                                args['is_superuser'] = 1

                            elif int(args['type']) == 1:
                                args['is_active'] = 1
                                args['is_staff'] = 1
                            else:
                                args['is_active'] = 1

                            number = args['number']
                            del args['number'], args['type'], args['csrfmiddlewaretoken']
                            user = User(**args)
                            password = random.randint(0, 999999999999)
                            user.set_password(password)
                            user_meta = userMeta()
                            try:
                                user.save()
                                send_sms(number, "You user name is "+args['username']+ " and "+str(password)+"  is your temporal password ignore this if not relevant", 'Transgov')
                                print password
                            except:
                                errors.append("Sorry could not create account")
                            user_meta.userId = user
                            user_meta.number = number
                            try:
                                user_meta.save()
                            except:
                                errors.append("Sorry something went wrong creating the account")
                            print ""

                            return HttpResponseRedirect("/account/")

                        else:
                            errors.append('Sorry but password dont match ')
                        context = {
                            'errors': errors,
                            'info': info,
                        }
                    else:
                        for error in v.errors:
                            errors.append(error + ':' + str(v.errors[error]))
                        context = {
                            'errors': errors,
                            'info': info,
                        }


    else:
        return HttpResponseRedirect('/project/')
    return render(request, 'addAccount.html', context)

@login_required
def edit(request, edit_id):
    if request.user.is_authenticated and request.user.is_superuser:
        context = {

        }
        try:
            if request.resolver_match.url_name == "subscribers_edit_account":
                redirect_url = "/account/subscribers"
            elif request.resolver_match.url_name == "staff_edit_account":
                redirect_url = "/account/staff"
            else:
                return HttpResponseRedirect('/project/')
            user = User.objects.filter(id=edit_id, is_superuser=0).get()
            if int(user.is_active) == 1:
                user.is_active = 0
            else:
                user.is_active = 1
            user.save()
            context['error'] = 'updated user successfully'
            context['error_code'] = 'green'
        except:
            context['error'] = 'Could not edit user account'
            context['error_code'] = 'red'
            return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/project/')
    return HttpResponseRedirect(redirect_url, context)


@login_required
def delete(request, delete_id):
    if request.user.is_authenticated and request.user.is_superuser:
        context = {

        }
        try:
            if request.resolver_match.url_name == "subscribers_delete_account":
                redirect_url = "/account/subscribers"
            elif request.resolver_match.url_name == "staff_delete_account":
                redirect_url = "/account/staff"
            else:
                return HttpResponseRedirect('/project/')
            User.objects.filter(id=delete_id,is_superuser=0).delete()
            context['error'] = 'Deleted user successfully'
            context['error_code'] = 'green'
        except:
            context['error'] = 'Could not delete user account'
            context['error_code'] = 'red'
            return HttpResponseRedirect(redirect_url)

    else:
        return HttpResponseRedirect('/project/')
    return HttpResponseRedirect(redirect_url,context)

@login_required
def profile(request):
    context = {

    }
    if request.user.is_authenticated:
        if request.method == "POST":
            args = {}
            user = User.objects.filter(id=request.user.id).get()
            valid = user.check_password(request.POST['old_password'])
            if valid:
                for element in request.POST:
                    args[element] = request.POST[element]

                if request.POST['old_password'] != "" and request.POST['password'] == request.POST['confirm_password']:
                    user.set_password(args['password'])
                    password = args['password']
                    del args['confirm_password'], args['old_password'], args['password']
                    try:
                        for element in args:
                            setattr(user, element, args[element])
                            user.save()
                            user = authenticate(username=args['username'],
                                                password=password)
                            login(request, user)
                            context['error'] = 'Profile updated successfully'
                            context['error_code'] = 'green'
                    except:
                            context['error'] = 'Could not update profile'
                            context['error_code'] = 'red'
            else:
                context['error'] = 'Sorry please provide a valid (old) password '
                context['error_code'] = 'red'
        profile = User.objects.filter(id=request.user.id).get()
        number = userMeta.objects.filter(userId_id=request.user.id).get()
        context['profile'] = profile
        context['number'] = number.number
    else:
        return HttpResponseRedirect('/project/')

    return render(request,'profile.html', context)
