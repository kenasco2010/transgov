from django.conf.urls import url, include
from all_auth.compat import importlib

from all_auth.socialaccount import providers

from . import app_settings

urlpatterns = [url('^', include('all_auth.account.urls'))]

if app_settings.SOCIALACCOUNT_ENABLED:
    urlpatterns += [url('^social/', include('all_auth.socialaccount.urls'))]

for provider in providers.registry.get_list():
    try:
        prov_mod = importlib.import_module(provider.get_package() + '.urls')
    except ImportError:
        continue
    prov_urlpatterns = getattr(prov_mod, 'urlpatterns', None)
    if prov_urlpatterns:
        urlpatterns += prov_urlpatterns
