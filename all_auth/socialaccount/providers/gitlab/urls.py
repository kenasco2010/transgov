# -*- coding: utf-8 -*-
from all_auth.socialaccount.providers.gitlab.provider import GitLabProvider
from all_auth.socialaccount.providers.oauth2.urls import default_urlpatterns


urlpatterns = default_urlpatterns(GitLabProvider)
