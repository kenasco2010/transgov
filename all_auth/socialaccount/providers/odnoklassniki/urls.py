from all_auth.socialaccount.providers.oauth2.urls import default_urlpatterns
from .provider import OdnoklassnikiProvider

urlpatterns = default_urlpatterns(OdnoklassnikiProvider)
