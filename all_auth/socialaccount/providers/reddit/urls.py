from all_auth.socialaccount.providers.oauth2.urls import default_urlpatterns
from .provider import RedditProvider

urlpatterns = default_urlpatterns(RedditProvider)
