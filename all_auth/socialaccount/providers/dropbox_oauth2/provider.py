from all_auth.socialaccount import providers
from all_auth.socialaccount.providers.base import ProviderAccount
from all_auth.socialaccount.providers.oauth2.provider import OAuth2Provider


class DropboxOAuth2Account(ProviderAccount):
    pass


class DropboxOAuth2Provider(OAuth2Provider):
    id = 'dropbox_oauth2'
    name = 'Dropbox'
    account_class = DropboxOAuth2Account

    def extract_uid(self, data):
        return data['uid']

    def extract_common_fields(self, data):
        return dict(name=data.get('display_name'),
                    email=data.get('email'))

providers.registry.register(DropboxOAuth2Provider)
