# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import all_auth.socialaccount.fields


class Migration(migrations.Migration):

    dependencies = [
        ('socialaccount', '0002_token_max_lengths'),
    ]

    operations = [
        migrations.AlterField(
            model_name='socialaccount',
            name='extra_data',
            field=all_auth.socialaccount.fields.JSONField(default=dict, verbose_name='extra data'),
            preserve_default=True,
        ),
    ]
