#!/usr/bin/env python
import urllib2
import urllib

def send_sms(phone,message,sender_id):
    api_key = '7ef49b17233a546d4e58'
    #parameters to send SMS
    params = {"key":api_key,"to":phone,"msg":message,"sender_id":sender_id}
    #url to send SMS
    url = 'https://apps.mnotify.net/smsapi?'+ urllib.urlencode(params)
    content = urllib2.urlopen(url).read()

    #Interpreting codes obtained from reading the URL
    if(content.strip() == '1000') :
        print "Message successfully sent"
    elif(content.strip() == '1002') :
        print "Message not sent"
    elif(content.strip() == '1003') :
        print "Your balance is not enough"
    elif(content.strip() == '1004') :
        print "Invalid API Key"
    elif(content.strip() == '1005') :
        print "Phone number not valid"
    elif(content.strip() == '1006') :
        print "Invalid sender id"
    elif(content.strip() == '1008') :
        print "Empty message"
